import { Component, OnInit } from '@angular/core';
import { User } from '../shared/user.model';
import { AuthLogService } from '../services/auth-log.service';
import { LocalStorageService } from '../services/local-storage.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  token: string;
  currentUser: User;
  user: object[];

  constructor(private authLogService: AuthLogService, private localStorageService: LocalStorageService) {
  }

  ngOnInit() {
    this.token = this.localStorageService.token();
  }

  logout(): void {
    this.token = null;
    this.localStorageService.logout();
    this.authLogService.logout().subscribe((data: object[]) => {
      this.user = data;
    });
  }

  userInfo(): void {
    if (this.token != null) {
      this.currentUser = this.localStorageService.currentUser();
    }
  }

}
