import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-task5',
  templateUrl: './task5.component.html',
  styleUrls: ['./task5.component.css']
})
export class Task5Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  themeChanger(): void {
    const hr = document.getElementsByTagName('hr');
    const modal1 = document.getElementById('id01');
    const modal2 = document.getElementById('id02');
    for (let i = 0; i < hr.length; i++) {
      hr[i].classList.toggle('dark-theme');
    }
    const themeBtn = document.getElementById('change-theme-btn');
    themeBtn.classList.toggle('dark-theme');
    modal1.classList.toggle('dark-theme');
    modal2.classList.toggle('dark-theme');
    document.body.classList.toggle('dark-theme');
  }

}
