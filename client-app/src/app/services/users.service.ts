import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
 providedIn: 'root'
})
export class UsersService {

  constructor(private httpClient: HttpClient) {
  }

  public getUsers(): any {
    return this.httpClient.get('http://192.168.1.99:3000/users');
  }
}
