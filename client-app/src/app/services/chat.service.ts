import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { Observable } from 'rxjs';

@Injectable({
 providedIn: 'root'
})
export class ChatService {

  private socket = io('http://192.168.1.99:3000/');

  joinRoom(data): void {
    this.socket.emit('join', data);
  }

  newUserJoined(): any {
    const observable = new Observable <{ user: string, message: string}> (observer => {
      this.socket.on('new user joined', (data) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }

  leaveRoom(data): void {
    this.socket.emit('leave', data);
  }

  userLeftRoom(): any {
    const observable = new Observable <{ user: string, message: string}> (observer => {
      this.socket.on('left room', (data) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }

  sendMessage(data): void {
    this.socket.emit('message', data);
  }

  newMessageReceived(): any {
    const observable = new Observable <{ user: string, message: string}> (observer => {
      this.socket.on('new message', (data) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }
}
