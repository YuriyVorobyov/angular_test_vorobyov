import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
 providedIn: 'root'
})
export class RandomService {

  myNumber$  = new Subject <number> ();

  get getNumber(): Observable<number> {
    return this.myNumber$.asObservable();
  }

  newNum(num: number): void {
    this.myNumber$.next(num);
  }
}
