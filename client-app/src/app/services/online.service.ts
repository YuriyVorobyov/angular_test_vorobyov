import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, interval, of, fromEvent, merge } from 'rxjs';
import { switchMap, mapTo} from 'rxjs/operators';

@Injectable({
 providedIn: 'root'
})
export class OnlineService {

  online$: Observable<boolean>;
  inter = interval(10000);

  constructor(private http: HttpClient) {
  }

  isUserLoggedIn$ = new Observable<boolean>((observer) => {
    const currentUser = localStorage.getItem('jwtToken');
    if (currentUser != null) {
      observer.next(true);
    } else {
      observer.next(false);
    }
  });

  getOnlineStatus(): Observable<boolean> {
    return this.online$ = merge(
      of(navigator.onLine),
      fromEvent(window, 'online').pipe(mapTo(true)),
      fromEvent(window, 'offline').pipe(mapTo(false))
    );
  }

  longPolling(): Observable<any> {
    return this.inter
    .pipe( switchMap(() => {
      return this.http.get('http://192.168.1.99:3000/polling/connect');
    }));
  }
}
