import { Injectable } from '@angular/core';
import { Observable, of, merge } from 'rxjs';
import { mapTo } from 'rxjs/operators';

type Message = string;

@Injectable({
 providedIn: 'root'
})
export class SearchService {

  search$: Observable<Message[]>;

  searchMessages(words: Message[], keyword: string): Observable<Message[]> {
    if (!words) {
      return this.search$ = merge(
        of(words)
      ).pipe(mapTo([]));
    }
    if (!keyword) {
      return this.search$ = merge(
        of(words)
      ).pipe(mapTo(words));
    }
    return this.search$ = merge(
      of(words)
    ).pipe(mapTo(
      words.filter(item =>
        item.includes(keyword)
        )
      )
    );
  }

}
