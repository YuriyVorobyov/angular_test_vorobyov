import { Injectable } from '@angular/core';

@Injectable({
 providedIn: 'root'
})
export class LocalStorageService {

  public setItems(data): void {
    localStorage.setItem('jwtToken', JSON.stringify(data.token));
    localStorage.setItem('user', JSON.stringify(data));
  }

  public currentUser(): any {
    return JSON.parse(localStorage.getItem('user'));
  }

  public token(): any {
    return localStorage.getItem('jwtToken');
  }

  public logout(): any {
    localStorage.removeItem('jwtToken');
    localStorage.removeItem('user');
  }
}
