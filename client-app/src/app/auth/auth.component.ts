import { Component, OnInit } from '@angular/core';
import { User } from '../shared/user.model';
import { AuthLogService } from '../services/auth-log.service';
import { LocalStorageService } from '../services/local-storage.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  user: User = new User();
  token: string;
  receivedUser: User;
  done: boolean;

  constructor(private authLogService: AuthLogService, private localStorageService: LocalStorageService) {
  }

  ngOnInit() {
    this.user.login = '';
    this.user.password = '';
  }

  submit(user: User): void {
    this.done = false;
    this.authLogService.postData(user)
      .subscribe(
        (data: User) => {
          this.done = true;
          this.receivedUser = data;
          this.localStorageService.setItems(data);
          const alert = document.getElementById('alert-good');
          alert.className = 'show';
          const modal = document.getElementById('id01');
          setTimeout(() => {
              alert.className = alert.className.replace('show', '');
           }, 1500);
          setTimeout(() => {
              modal.style.display = 'none';
          }, 1000);
        },
        (error) => {
          const alert = document.getElementById('alert-inv');
          alert.className = 'show';
          setTimeout(() => {
            alert.className = alert.className.replace('show', '');
           }, 3000);
        }
      );
  }

  badInfo(): void {
    const alert = document.getElementById('alert-bad');
    alert.className = 'show';
    setTimeout(() => {
      alert.className = alert.className.replace('show', '');
     }, 3000);
  }

  clearInput(): void {
    this.user.login = '';
    this.user.password = '';
  }
}
