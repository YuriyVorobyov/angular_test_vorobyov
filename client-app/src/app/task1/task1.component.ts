import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { OnlineService } from '../services/online.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-task1',
  templateUrl: './task1.component.html',
  styleUrls: ['./task1.component.css']
})
export class Task1Component implements OnInit {

  onlineChecker: boolean;
  subscription: Subscription;

  constructor(private http: HttpClient, private onlineService: OnlineService) {
  }

  ngOnInit() {
    this.getOnlineStatus();
  }

  getOnlineStatus(): void  {
    this.onlineService.getOnlineStatus()
      .subscribe(value => {
        this.onlineChecker = value;
        if (this.onlineChecker === true) {
          this.longPolling();
        } else {
          this.stopPolling();
        }
      });
  }

  longPolling(): void {
    this.subscription = this.onlineService.longPolling()
      .subscribe();
  }

  stopPolling() {
    this.subscription.unsubscribe();
  }

}
