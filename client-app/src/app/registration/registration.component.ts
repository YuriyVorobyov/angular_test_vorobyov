import { Component, OnInit } from '@angular/core';
import { User } from '../shared/user.model';
import { AuthRegService } from '../services/auth-reg.service';
import { LocalStorageService } from '../services/local-storage.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  user: User = new User();
  receivedUser: User;
  done = false;
  token: string;
  errorBoolean = false;

  constructor(private authRegService: AuthRegService, private localStorageService: LocalStorageService) {
  }

  ngOnInit() {
    this.user.login = '';
    this.user.password = '';
    this.errorBoolean = false;
  }

  submit(user: User): void {

    this.errorBoolean = false;

    if (user.login.trim().length === 0 || user.password.trim().length === 0) {
      this.badInfo();
      this.errorBoolean = true;
    } else {
      this.authRegService.postData(user)
        .subscribe(
          (data: User) => {
            this.receivedUser = data;
            this.done = true;
            this.localStorageService.setItems(data);
            const alert = document.getElementById('alert-good-reg');
            alert.className = 'show';
            const modal = document.getElementById('id02');
            setTimeout(() => { alert.className = alert.className.replace('show', ''); }, 1500);
            setTimeout(() => {
                modal.style.display = 'none';
            }, 1000);
          },
          (error) => {
            if (this.errorBoolean === false) {
              this.invInfo();
            }
          }
        );
      }
  }

  badInfo(): void {
    const alert = document.getElementById('alert-bad-reg');
    alert.className = 'show';
    setTimeout(() => { alert.className = alert.className.replace('show', ''); }, 3000);
  }

  goodInfo(): void {
    const alert = document.getElementById('alert-good-reg');
    alert.className = 'show';
    const modal = document.getElementById('id02');
    setTimeout(() => { alert.className = alert.className.replace('show', ''); }, 1500);
    setTimeout(() => { modal.style.display = 'none'; }, 1500);
  }

  invInfo(): void {
    const alert = document.getElementById('alert-inv-reg');
    alert.className = 'show';
    setTimeout(() => { alert.className = alert.className.replace('show', ''); }, 3000);
  }

  clearInput(): void {
    this.user.login = '';
    this.user.password = '';
  }

}
