import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { RandomService } from '../services/random.service';

@Component({
  selector: 'app-task3',
  templateUrl: './task3.component.html',
  styleUrls: ['./task3.component.css']
})
export class Task3Component implements OnInit {

  subscription: Subscription;
  interval;
  randomNum = [];
  displayResult: object;

  constructor(private random: RandomService) {
  }

  ngOnInit() {
    this.displayResult = document.querySelector('.result-array');
  }

  getNumber(): void {
    this.subscription = this.random.getNumber.subscribe((num: number) => {
      this.randomNum.push(num);
    });

    this.interval = setInterval(() => {
      this.displayResult['textContent'] = this.randomNum;
    }, 2000);

  }

  stop(): void {
    this.subscription.unsubscribe();
    clearInterval(this.interval);
  }

  setNum(): void {
    this.interval = setInterval(() => {
      this.random.newNum(Math.floor(Math.random() * 10));
    }, 500);
  }

}
