import { Component, OnInit, DoCheck } from '@angular/core';
import { OnlineService } from '../services/online.service';

@Component({
  selector: 'app-task2',
  templateUrl: './task2.component.html',
  styleUrls: ['./task2.component.css']
})
export class Task2Component implements OnInit, DoCheck {

  onlineChecker: boolean;
  userChecker: boolean;

  constructor(private onlineService: OnlineService) { }

  ngOnInit() {
    this.online();
    this.getUser();
  }

  ngDoCheck() {
    this.getUser();
  }

  online(): void  {
    this.onlineService.getOnlineStatus()
      .subscribe(value => {
        this.onlineChecker = value;
      });
  }

  getUser(): void {
    this.onlineService.isUserLoggedIn$.subscribe((value) => {
      this.userChecker = value;
    });
  }

}
