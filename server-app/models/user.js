'use strict';

const bcrypt = require('bcrypt-nodejs');
const BCRYPTVAL = 10;

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    login: DataTypes.STRING,
    password: DataTypes.STRING,
  }, {});


  User.beforeSave(user => {
    if (user.changed('password')) {
      // eslint-disable-next-line no-sync
      user.password = bcrypt.hashSync(user.password, bcrypt.genSaltSync(BCRYPTVAL), null);
    }
  });
  User.prototype.comparePassword = function(passw, cb) {
    bcrypt.compare(passw, this.password, (err, isMatch) => {
      if (err) {
        return cb(err);
      }
      cb(null, isMatch);
    });
  };

  return User;
};
