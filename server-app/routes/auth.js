const express = require('express');
const router = new express.Router();
const authController = require('../controllers/auth');

router.post('/signup', authController.signUp);
router.post('/signin', authController.signIn);

router.get('/logout', (req, res) => {
  req.logout();
  req.session.destroy(() => {
    res.clearCookie('connect.sid');
    res.redirect('/');
  });
});

module.exports = router;
