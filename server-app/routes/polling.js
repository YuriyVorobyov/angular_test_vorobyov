const express = require('express');
const router = new express.Router();
const DELAY = 2000;

router.get('/connect', (req, res) => {
  setTimeout(() => res.send(JSON.stringify('Long polling answer')), DELAY);
});

module.exports = router;
