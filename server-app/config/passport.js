const JwtStrategy = require('passport-jwt').Strategy;
const { ExtractJwt } = require('passport-jwt');

// Load up the user model
const { Users } = require('../models');

module.exports = function(passport) {
  const opts = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('JWT'),
    secretOrKey: 'nodeauthsecret',
  };


  passport.use('jwt', new JwtStrategy(opts, (jwt_payload, done) => {
    Users
      .findByPk(jwt_payload.id)
      .then(user => done(null, user))
      .catch(error => done(error, false));
  }));
};
