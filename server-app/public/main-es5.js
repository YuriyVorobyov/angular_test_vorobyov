(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"], {
        /***/ "./$$_lazy_route_resource lazy recursive": 
        /*!******************************************************!*\
          !*** ./$$_lazy_route_resource lazy namespace object ***!
          \******************************************************/
        /*! no static exports found */
        /***/ (function (module, exports) {
            function webpackEmptyAsyncContext(req) {
                // Here Promise.resolve().then() is used instead of new Promise() to prevent
                // uncaught exception popping up in devtools
                return Promise.resolve().then(function () {
                    var e = new Error("Cannot find module '" + req + "'");
                    e.code = 'MODULE_NOT_FOUND';
                    throw e;
                });
            }
            webpackEmptyAsyncContext.keys = function () { return []; };
            webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
            module.exports = webpackEmptyAsyncContext;
            webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html": 
        /*!**************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
          \**************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<h1 class=\"title\">Test Angular</h1>\n<hr id=\"line\">  \n<h2 class=\"title-small\">#1</h2>\n<h3 class=\"title-small\">Task: Long polling</h3>\n<app-task1></app-task1>\n<hr id=\"line\">  \n<h2 class=\"title-small\">#2</h2>\n<h3 class=\"title-small\">Task: Detect when user is online and logged in</h3>\n<app-task2>\n  <div class=\"button-block-auth\">\n      <app-auth></app-auth>\n      <app-registration></app-registration>\n      <app-logout></app-logout> \n  </div>  \n</app-task2>\n<hr id=\"line\">  \n<h2 class=\"title-small\">#3</h2>\n<h3 class=\"title-small\">Task: Function adds a random number every 500ms and get list of them each 2s</h3>\n<app-task3></app-task3>\n<hr id=\"line\"> \n<h2 class=\"title-small\">#4</h2>\n<h3 class=\"title-small\">Task: Input change</h3>\n<app-task4></app-task4>\n<hr id=\"line\"> \n<h2 class=\"title-small\">#5</h2>\n<h3 class=\"title-small\">Task: Multiple themes structure</h3>\n<app-task5></app-task5>\n<hr id=\"line\"> \n<h2 class=\"title-small\">#6</h2>\n<h3 class=\"title-small\">Task: Conversation and Message models</h3>\n<app-task6></app-task6>");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/auth/auth.component.html": 
        /*!********************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/auth/auth.component.html ***!
          \********************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<button onclick=\"document.getElementById('id01').style.display='block'\">Login</button>\n\n<div id=\"id01\" class=\"modal\">\n  <form class=\"modal-content animate\">\n    <div class=\"imgcontainer\">\n      <span (click)=\"clearInput()\" onclick=\"document.getElementById('id01').style.display='none'\" class=\"close\" title=\"Close\">&times;</span>\n    </div>\n\n    <div class=\"container\">\n      <label for=\"login\"><b>Email</b></label>\n      <input type=\"text\" autocomplete=\"off\" placeholder=\"Enter login\" name=\"login\" [(ngModel)]=\"user.login\" required>\n\n      <label for=\"psw\"><b>Password</b></label>\n      <input type=\"password\" autocomplete=\"off\" placeholder=\"Enter Password\" name=\"password\" [(ngModel)]=\"user.password\" required>\n      \n      <div *ngIf=\"user.login == '' || user.password == '';else buttonOk\">\n        <button type=\"submit\" (click)=\"badInfo()\" class=\"signinbtn\" style=\"background-color:grey\" >Login</button>\n      </div>\n\n      <ng-template #buttonOk>\n        <button type=\"submit\" (click)=\"submit(user)\"  class=\"signinbtn\" >Login</button>\n      </ng-template>\n\n    </div>\n\n    <div id=\"alert-bad\">\n      <strong>Attention!</strong> Enter all user data!\n    </div>\n\n    <div id=\"alert-good\">\n      <strong>Success!</strong> You are authorized! This window will close automatically.\n    </div>\n\n    <div id=\"alert-inv\">\n      <strong>Warning!</strong> Invalid login or password.\n    </div>\n\n    <div class=\"container\" style=\"background-color:#f1f1f1\">\n      <button type=\"button\" (click)=\"clearInput()\" onclick=\"document.getElementById('id01').style.display='none'\" class=\"cancelbtn\">Cancel</button>\n    </div>\n  </form>\n</div>\n\n");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/logout/logout.component.html": 
        /*!************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/logout/logout.component.html ***!
          \************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<button (click)=\"logout()\" class=\"logout\">Logout</button>\n");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/registration/registration.component.html": 
        /*!************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/registration/registration.component.html ***!
          \************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<button onclick=\"document.getElementById('id02').style.display='block'\">Sign Up</button>\n\n<div id=\"id02\" class=\"modal\">\n  \n  <form class=\"modal-content animate\">\n    <span (click)=\"clearInput()\" onclick=\"this.form.reset()\" onclick=\"document.getElementById('id02').style.display='none'\" class=\"close\" title=\"Close\">&times;</span>\n    <div class=\"container\">\n      <h1>Sign Up</h1>\n      <p>Please fill in this form to create an account.</p>\n      <hr>\n      <label for=\"login\"><b>Login</b></label>\n      <input type=\"text\" autocomplete=\"off\" placeholder=\"Enter login\" name=\"login\" [(ngModel)]=\"user.login\" required>\n\n      <label for=\"password\"><b>Password</b></label>\n      <input type=\"password\" autocomplete=\"off\" placeholder=\"Enter Password\" name=\"password\" [(ngModel)]=\"user.password\" required>\n\n      <p>By creating an account you agree to our <a href=\"#\" style=\"color:dodgerblue\">Terms & Privacy</a>.</p>\n\n      <div *ngIf=\"user.login == '' || user.password == '';else buttonOk\">\n        <button type=\"submit\" (click)=\"badInfo()\" class=\"signupbtn\" style=\"background-color:grey\" >Sign Up</button>\n      </div>\n\n      <ng-template #buttonOk>\n        <button type=\"submit\" (click)=\"submit(user)\" class=\"signupbtn\" >Sign Up</button>\n      </ng-template>\n    </div>\n\n    <div id=\"alert-bad-reg\">\n      <strong>Attention!</strong> Enter all user data!\n    </div>\n    \n    <div id=\"alert-good-reg\">\n      <strong>Success!</strong> You will be logged in automatically! This window will close automatically.\n    </div>\n\n    <div id=\"alert-inv-reg\">\n      <strong>Warning!</strong> This login already in use.\n    </div>\n\n    <div class=\"container\" style=\"background-color:#f1f1f1\">\n      <button type=\"button\" (click)=\"clearInput()\" onclick=\"this.form.reset()\" onclick=\"document.getElementById('id02').style.display='none'\" style=\"width:auto\" class=\"cancelbtn\">Cancel</button>\n    </div>\n  </form>\n  \n</div>\n\n\n");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/task1/task1.component.html": 
        /*!**********************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/task1/task1.component.html ***!
          \**********************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div *ngIf=\"onlineChecker;else noConnection\">\n    <div class=\"info-text\">\n        <p>Internet connection...</p>\n        <p class=\"req\">Requests are sent</p>\n    </div>\n</div>\n\n<ng-template #noConnection>\n    <div class=\"info-text\">\n        <p>No internet connection...</p>\n        <p>Requests are not sent</p>\n    </div>    \n</ng-template>");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/task2/task2.component.html": 
        /*!**********************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/task2/task2.component.html ***!
          \**********************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (" \n<ng-content></ng-content>\n\n<div *ngIf=\"onlineChecker && userChecker\">\n    <div class=\"info-text\">\n        <p>User is online and logged in</p>\n    </div>\n</div>\n");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/task3/task3.component.html": 
        /*!**********************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/task3/task3.component.html ***!
          \**********************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"button-block-random\">\n    <button (click)=\"setNum()\" (click)=\"getNumber()\">Start</button>\n    <button (click)=\"stop()\">Stop</button>\n</div>\n\n<div class=\"info-text\">\n    <p>Result: </p>\n    <p class=\"result-array\"></p>\n</div>");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/task4/task4.component.html": 
        /*!**********************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/task4/task4.component.html ***!
          \**********************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<input type=\"text\" autocomplete=\"off\" placeholder=\"Enter text for searching\" [(ngModel)]=\"keyword\" name=\"text\">\n\n<div class=\"words-block\">\n    <div *ngFor=\"let item of words\" class=\"words-block__item\">\n        <p class=\"news-block__title\">{{item}}</p>\n    </div> \n</div>");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/task5/task5.component.html": 
        /*!**********************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/task5/task5.component.html ***!
          \**********************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"change-button-block\">\n    <button (click)=\"themeChanger()\" id=\"change-theme-btn\">theme</button>\n</div>\n");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/task6/task6.component.html": 
        /*!**********************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/task6/task6.component.html ***!
          \**********************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"container-fluid\">\n    <div class=\"row\">\n        <div class=\"col-sm-5\">\n            <div class=\"form-group\">\n                <label for=\"username\">Username</label>\n                <input type=\"text\" [(ngModel)]=\"user\" class=\"form-control\" id=\"username\" placeholder=\"Enter username\">\n            </div>\n        </div>\n        <div class=\"col-sm-5\">\n            <label for=\"roomInput\">Choose conversation</label>\n            <select name=\"roomInput\" id=\"roomInput\" class=\"form-control\" [(ngModel)]=\"room\">\n                <option value=\"Test1\">Conversation_1</option>\n            </select>\n        </div>\n        <div class=\"col sm-2\">\n            <div class=\"button-ctrl-block\" class=\"pull-right\">\n                <div *ngIf=\"joined == true;else buttonOkJoin\"> \n                    <button type=\"button\" style=\"background-color:grey\">Join</button>\n                </div>\n                <ng-template #buttonOkJoin>\n                    <button type=\"button\" (click)=\"join()\">Join</button>\n                </ng-template>  \n                <div *ngIf=\"joined == false;else buttonOkLeave\"> \n                    <button type=\"button\" style=\"background-color:grey\">Leave</button>\n                </div>\n                <ng-template #buttonOkLeave>\n                    <button type=\"button\" (click)=\"leave()\">Leave</button>\n                </ng-template>  \n            </div>\n        </div>\n    </div>\n\n    <div class=\"row\" class=\"window\">\n        <div class=\"well\">\n            <div *ngFor=\"let item of messageArray\">\n            <span><strong>{{item.user}} : </strong> {{item.message}}</span>\n            </div>\n        </div>\n    </div>\n    <div class=\"row\">\n        <div class=\"col-sm-10\">\n            <input type=\"text\" class=\"form-control\" [(ngModel)]=\"messageText\">            \n        </div>\n        <div class=\"col-sm-2\">\n            <div *ngIf=\"user == '' || room == '' || joined == false;else buttonOk\">\n                <button type=\"button\" class=\"pull-right\" style=\"background-color:grey\">Send</button>\n            </div>\n            <ng-template #buttonOk>\n                <button type=\"button\" class=\"pull-right\" (click)=\"sendMessage()\" (click)=\"clearInput()\">Send</button>\n            </ng-template>    \n        </div>\n    </div>\n</div>\n");
            /***/ 
        }),
        /***/ "./node_modules/tslib/tslib.es6.js": 
        /*!*****************************************!*\
          !*** ./node_modules/tslib/tslib.es6.js ***!
          \*****************************************/
        /*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__extends", function () { return __extends; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__assign", function () { return __assign; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__rest", function () { return __rest; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__decorate", function () { return __decorate; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__param", function () { return __param; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__metadata", function () { return __metadata; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__awaiter", function () { return __awaiter; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__generator", function () { return __generator; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__exportStar", function () { return __exportStar; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__values", function () { return __values; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__read", function () { return __read; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spread", function () { return __spread; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spreadArrays", function () { return __spreadArrays; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__await", function () { return __await; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function () { return __asyncGenerator; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function () { return __asyncDelegator; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncValues", function () { return __asyncValues; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function () { return __makeTemplateObject; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importStar", function () { return __importStar; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importDefault", function () { return __importDefault; });
            /*! *****************************************************************************
            Copyright (c) Microsoft Corporation. All rights reserved.
            Licensed under the Apache License, Version 2.0 (the "License"); you may not use
            this file except in compliance with the License. You may obtain a copy of the
            License at http://www.apache.org/licenses/LICENSE-2.0
            
            THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
            KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
            WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
            MERCHANTABLITY OR NON-INFRINGEMENT.
            
            See the Apache Version 2.0 License for specific language governing permissions
            and limitations under the License.
            ***************************************************************************** */
            /* global Reflect, Promise */
            var extendStatics = function (d, b) {
                extendStatics = Object.setPrototypeOf ||
                    ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
                    function (d, b) { for (var p in b)
                        if (b.hasOwnProperty(p))
                            d[p] = b[p]; };
                return extendStatics(d, b);
            };
            function __extends(d, b) {
                extendStatics(d, b);
                function __() { this.constructor = d; }
                d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
            }
            var __assign = function () {
                __assign = Object.assign || function __assign(t) {
                    for (var s, i = 1, n = arguments.length; i < n; i++) {
                        s = arguments[i];
                        for (var p in s)
                            if (Object.prototype.hasOwnProperty.call(s, p))
                                t[p] = s[p];
                    }
                    return t;
                };
                return __assign.apply(this, arguments);
            };
            function __rest(s, e) {
                var t = {};
                for (var p in s)
                    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
                        t[p] = s[p];
                if (s != null && typeof Object.getOwnPropertySymbols === "function")
                    for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                        if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                            t[p[i]] = s[p[i]];
                    }
                return t;
            }
            function __decorate(decorators, target, key, desc) {
                var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
                if (typeof Reflect === "object" && typeof Reflect.decorate === "function")
                    r = Reflect.decorate(decorators, target, key, desc);
                else
                    for (var i = decorators.length - 1; i >= 0; i--)
                        if (d = decorators[i])
                            r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
                return c > 3 && r && Object.defineProperty(target, key, r), r;
            }
            function __param(paramIndex, decorator) {
                return function (target, key) { decorator(target, key, paramIndex); };
            }
            function __metadata(metadataKey, metadataValue) {
                if (typeof Reflect === "object" && typeof Reflect.metadata === "function")
                    return Reflect.metadata(metadataKey, metadataValue);
            }
            function __awaiter(thisArg, _arguments, P, generator) {
                return new (P || (P = Promise))(function (resolve, reject) {
                    function fulfilled(value) { try {
                        step(generator.next(value));
                    }
                    catch (e) {
                        reject(e);
                    } }
                    function rejected(value) { try {
                        step(generator["throw"](value));
                    }
                    catch (e) {
                        reject(e);
                    } }
                    function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
                    step((generator = generator.apply(thisArg, _arguments || [])).next());
                });
            }
            function __generator(thisArg, body) {
                var _ = { label: 0, sent: function () { if (t[0] & 1)
                        throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
                return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function () { return this; }), g;
                function verb(n) { return function (v) { return step([n, v]); }; }
                function step(op) {
                    if (f)
                        throw new TypeError("Generator is already executing.");
                    while (_)
                        try {
                            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done)
                                return t;
                            if (y = 0, t)
                                op = [op[0] & 2, t.value];
                            switch (op[0]) {
                                case 0:
                                case 1:
                                    t = op;
                                    break;
                                case 4:
                                    _.label++;
                                    return { value: op[1], done: false };
                                case 5:
                                    _.label++;
                                    y = op[1];
                                    op = [0];
                                    continue;
                                case 7:
                                    op = _.ops.pop();
                                    _.trys.pop();
                                    continue;
                                default:
                                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
                                        _ = 0;
                                        continue;
                                    }
                                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) {
                                        _.label = op[1];
                                        break;
                                    }
                                    if (op[0] === 6 && _.label < t[1]) {
                                        _.label = t[1];
                                        t = op;
                                        break;
                                    }
                                    if (t && _.label < t[2]) {
                                        _.label = t[2];
                                        _.ops.push(op);
                                        break;
                                    }
                                    if (t[2])
                                        _.ops.pop();
                                    _.trys.pop();
                                    continue;
                            }
                            op = body.call(thisArg, _);
                        }
                        catch (e) {
                            op = [6, e];
                            y = 0;
                        }
                        finally {
                            f = t = 0;
                        }
                    if (op[0] & 5)
                        throw op[1];
                    return { value: op[0] ? op[1] : void 0, done: true };
                }
            }
            function __exportStar(m, exports) {
                for (var p in m)
                    if (!exports.hasOwnProperty(p))
                        exports[p] = m[p];
            }
            function __values(o) {
                var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
                if (m)
                    return m.call(o);
                return {
                    next: function () {
                        if (o && i >= o.length)
                            o = void 0;
                        return { value: o && o[i++], done: !o };
                    }
                };
            }
            function __read(o, n) {
                var m = typeof Symbol === "function" && o[Symbol.iterator];
                if (!m)
                    return o;
                var i = m.call(o), r, ar = [], e;
                try {
                    while ((n === void 0 || n-- > 0) && !(r = i.next()).done)
                        ar.push(r.value);
                }
                catch (error) {
                    e = { error: error };
                }
                finally {
                    try {
                        if (r && !r.done && (m = i["return"]))
                            m.call(i);
                    }
                    finally {
                        if (e)
                            throw e.error;
                    }
                }
                return ar;
            }
            function __spread() {
                for (var ar = [], i = 0; i < arguments.length; i++)
                    ar = ar.concat(__read(arguments[i]));
                return ar;
            }
            function __spreadArrays() {
                for (var s = 0, i = 0, il = arguments.length; i < il; i++)
                    s += arguments[i].length;
                for (var r = Array(s), k = 0, i = 0; i < il; i++)
                    for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                        r[k] = a[j];
                return r;
            }
            ;
            function __await(v) {
                return this instanceof __await ? (this.v = v, this) : new __await(v);
            }
            function __asyncGenerator(thisArg, _arguments, generator) {
                if (!Symbol.asyncIterator)
                    throw new TypeError("Symbol.asyncIterator is not defined.");
                var g = generator.apply(thisArg, _arguments || []), i, q = [];
                return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
                function verb(n) { if (g[n])
                    i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
                function resume(n, v) { try {
                    step(g[n](v));
                }
                catch (e) {
                    settle(q[0][3], e);
                } }
                function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
                function fulfill(value) { resume("next", value); }
                function reject(value) { resume("throw", value); }
                function settle(f, v) { if (f(v), q.shift(), q.length)
                    resume(q[0][0], q[0][1]); }
            }
            function __asyncDelegator(o) {
                var i, p;
                return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
                function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
            }
            function __asyncValues(o) {
                if (!Symbol.asyncIterator)
                    throw new TypeError("Symbol.asyncIterator is not defined.");
                var m = o[Symbol.asyncIterator], i;
                return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
                function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
                function settle(resolve, reject, d, v) { Promise.resolve(v).then(function (v) { resolve({ value: v, done: d }); }, reject); }
            }
            function __makeTemplateObject(cooked, raw) {
                if (Object.defineProperty) {
                    Object.defineProperty(cooked, "raw", { value: raw });
                }
                else {
                    cooked.raw = raw;
                }
                return cooked;
            }
            ;
            function __importStar(mod) {
                if (mod && mod.__esModule)
                    return mod;
                var result = {};
                if (mod != null)
                    for (var k in mod)
                        if (Object.hasOwnProperty.call(mod, k))
                            result[k] = mod[k];
                result.default = mod;
                return result;
            }
            function __importDefault(mod) {
                return (mod && mod.__esModule) ? mod : { default: mod };
            }
            /***/ 
        }),
        /***/ "./src/app/app-routing.module.ts": 
        /*!***************************************!*\
          !*** ./src/app/app-routing.module.ts ***!
          \***************************************/
        /*! exports provided: AppRoutingModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function () { return AppRoutingModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            var routes = [];
            var AppRoutingModule = /** @class */ (function () {
                function AppRoutingModule() {
                }
                return AppRoutingModule;
            }());
            AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
                    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
                })
            ], AppRoutingModule);
            /***/ 
        }),
        /***/ "./src/app/app.component.css": 
        /*!***********************************!*\
          !*** ./src/app/app.component.css ***!
          \***********************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".button-block-auth {\n    display: flex;\n    justify-content: space-evenly;\n    width: 100%;\n}\n\nhr {\n    border: none;\n    background-color: black;\n    color: black;\n    height: 2px; \n}\n\n.dark-theme {\n    background-color: #333;\n}\n\nhr.dark-theme {\n    background-color: #fff;\n    color: #fff;\n}\n\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxhQUFhO0lBQ2IsNkJBQTZCO0lBQzdCLFdBQVc7QUFDZjs7QUFFQTtJQUNJLFlBQVk7SUFDWix1QkFBdUI7SUFDdkIsWUFBWTtJQUNaLFdBQVc7QUFDZjs7QUFFQTtJQUNJLHNCQUFzQjtBQUMxQjs7QUFFQTtJQUNJLHNCQUFzQjtJQUN0QixXQUFXO0FBQ2YiLCJmaWxlIjoic3JjL2FwcC9hcHAuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5idXR0b24tYmxvY2stYXV0aCB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWV2ZW5seTtcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuaHIge1xuICAgIGJvcmRlcjogbm9uZTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcbiAgICBjb2xvcjogYmxhY2s7XG4gICAgaGVpZ2h0OiAycHg7IFxufVxuXG4uZGFyay10aGVtZSB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzMzMztcbn1cblxuaHIuZGFyay10aGVtZSB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgICBjb2xvcjogI2ZmZjtcbn1cblxuIl19 */");
            /***/ 
        }),
        /***/ "./src/app/app.component.ts": 
        /*!**********************************!*\
          !*** ./src/app/app.component.ts ***!
          \**********************************/
        /*! exports provided: AppComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function () { return AppComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            var AppComponent = /** @class */ (function () {
                function AppComponent() {
                    this.title = 'client-app';
                }
                return AppComponent;
            }());
            AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-root',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")).default]
                })
            ], AppComponent);
            /***/ 
        }),
        /***/ "./src/app/app.module.ts": 
        /*!*******************************!*\
          !*** ./src/app/app.module.ts ***!
          \*******************************/
        /*! exports provided: AppModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function () { return AppModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
            /* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
            /* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
            /* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
            /* harmony import */ var _task2_task2_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./task2/task2.component */ "./src/app/task2/task2.component.ts");
            /* harmony import */ var _auth_auth_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./auth/auth.component */ "./src/app/auth/auth.component.ts");
            /* harmony import */ var _registration_registration_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./registration/registration.component */ "./src/app/registration/registration.component.ts");
            /* harmony import */ var _logout_logout_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./logout/logout.component */ "./src/app/logout/logout.component.ts");
            /* harmony import */ var _task3_task3_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./task3/task3.component */ "./src/app/task3/task3.component.ts");
            /* harmony import */ var _task4_task4_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./task4/task4.component */ "./src/app/task4/task4.component.ts");
            /* harmony import */ var _task6_task6_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./task6/task6.component */ "./src/app/task6/task6.component.ts");
            /* harmony import */ var _task5_task5_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./task5/task5.component */ "./src/app/task5/task5.component.ts");
            /* harmony import */ var _task1_task1_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./task1/task1.component */ "./src/app/task1/task1.component.ts");
            /* harmony import */ var ng2_search_filter__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ng2-search-filter */ "./node_modules/ng2-search-filter/ng2-search-filter.js");
            var AppModule = /** @class */ (function () {
                function AppModule() {
                }
                return AppModule;
            }());
            AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
                    declarations: [
                        _app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"],
                        _task2_task2_component__WEBPACK_IMPORTED_MODULE_7__["Task2Component"],
                        _auth_auth_component__WEBPACK_IMPORTED_MODULE_8__["AuthComponent"],
                        _registration_registration_component__WEBPACK_IMPORTED_MODULE_9__["RegistrationComponent"],
                        _logout_logout_component__WEBPACK_IMPORTED_MODULE_10__["LogoutComponent"],
                        _task3_task3_component__WEBPACK_IMPORTED_MODULE_11__["Task3Component"],
                        _task4_task4_component__WEBPACK_IMPORTED_MODULE_12__["Task4Component"],
                        _task6_task6_component__WEBPACK_IMPORTED_MODULE_13__["Task6Component"],
                        _task5_task5_component__WEBPACK_IMPORTED_MODULE_14__["Task5Component"],
                        _task1_task1_component__WEBPACK_IMPORTED_MODULE_15__["Task1Component"]
                    ],
                    imports: [
                        _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                        _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"],
                        _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
                        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                        ng2_search_filter__WEBPACK_IMPORTED_MODULE_16__["Ng2SearchPipeModule"]
                    ],
                    providers: [],
                    bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]]
                })
            ], AppModule);
            /***/ 
        }),
        /***/ "./src/app/auth/auth.component.css": 
        /*!*****************************************!*\
          !*** ./src/app/auth/auth.component.css ***!
          \*****************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("input[type=text], input[type=password] {\n  width: 100%;\n  padding: 12px 20px;\n  margin: 8px 0;\n  display: inline-block;\n  border: 1px solid #ccc;\n  box-sizing: border-box;\n  background-color: #f1f1f1;\n}\n\ninput[type=text]:focus, input[type=password]:focus {\n  background-color: #ddd;\n  outline: none;\n}\n\nbutton {\n  background-color: #2199e8;\n  color: white;\n  padding: 14px 20px;\n  margin: 8px 0;\n  margin-right: 1.0625rem;\n  border: none;\n  cursor: pointer;\n  width: 100%;\n}\n\nbutton:hover {\n  opacity: 0.8;\n}\n\n.cancelbtn {\nwidth: auto;\npadding: 10px 18px;\nbackground-color: #f44336;\n}\n\n.container {\n  padding: 16px;\n}\n\nspan.psw {\n  float: right;\n  padding-top: 16px;\n}\n\n.modal {\n  display: none;\n  position: fixed;\n  z-index: 1;\n  left: 0;\n  top: 0;\n  width: 100%;\n  height: 100%;\n  overflow: auto;\n  background-color: rgb(0,0,0);\n  background-color: rgba(0,0,0,0.4);\n  padding-top: 60px;\n}\n\n.modal-content {\n  background-color: #fefefe;\n  margin: 5% auto 15% auto;\n  border: 1px solid #888;\n  width: 80%;\n}\n\n.close {\n  position: absolute;\n  right: 25px;\n  top: 8px;\n  color: #000;\n  font-size: 35px;\n  font-weight: bold;\n}\n\n.close:hover,\n.close:focus {\n  color: red;\n  cursor: pointer;\n}\n\n.animate {\n  -webkit-animation: animatezoom 0.6s;\n  animation: animatezoom 0.6s\n}\n\n@-webkit-keyframes animatezoom {\n  from {-webkit-transform: scale(0)} \n  to {-webkit-transform: scale(1)}\n}\n\n@keyframes animatezoom {\n  from {transform: scale(0)} \n  to {transform: scale(1)}\n}\n\n@media screen and (max-width: 300px) {\n  span.psw {\n     display: block;\n     float: none;\n  }\n  .cancelbtn {\n     width: 100%;\n  }\n}\n\n#alert-bad {\n  visibility: hidden;\n  padding: 20px;\n  width: 79.1%;\n  margin-left: 10%;\n  margin-bottom: 3%;\n  background-color: #f44336;\n  color: white;\n  z-index: 1;\n}\n\n#alert-bad.show {\n  visibility: visible;\n}\n\n#alert-good {\n  visibility: hidden;\n  padding: 20px;\n  width: 79.1%;\n  margin-left: 10%;\n  margin-bottom: 3%;\n  background-color: #4CAF50;\n  color: white;\n  z-index: 1;\n}\n\n#alert-good.show {\n  visibility: visible;\n}\n\n#alert-inv {\n  visibility: hidden;\n  padding: 20px;\n  width: 79.1%;\n  margin-left: 10%;\n  margin-bottom: 3%;\n  background-color: #ff9800;\n  color: white;\n  z-index: 1;\n}\n\n#alert-inv.show {\n  visibility: visible;\n}\n\n@-webkit-keyframes fadein {\n  from {bottom: 0; opacity: 0;} \n  to {bottom: 30px; opacity: 1;}\n}\n\n@keyframes fadein {\n  from {bottom: 0; opacity: 0;}\n  to {bottom: 30px; opacity: 1;}\n}\n\n@-webkit-keyframes fadeout {\n  from {bottom: 30px; opacity: 1;} \n  to {bottom: 0; opacity: 0;}\n}\n\n@keyframes fadeout {\n  from {bottom: 30px; opacity: 1;}\n  to {bottom: 0; opacity: 0;}\n}\n\n#alert-bad-2 {\n  visibility: hidden;\n  padding: 20px;\n  width: 79.1%;\n  margin-left: 10%;\n  margin-bottom: 3%;\n  background-color: #f44336;\n  color: white;\n  z-index: 1;\n}\n\n#alert-bad-2.show {\n  visibility: visible;\n}\n\n#alert-good-2 {\n  visibility: hidden;\n  padding: 20px;\n  width: 79.1%;\n  margin-left: 10%;\n  margin-bottom: 3%;\n  background-color: #4CAF50;\n  color: white;\n  z-index: 1;\n}\n\n#alert-good-2.show {\n  visibility: visible;\n}\n\n#alert-inv-2 {\n  visibility: hidden;\n  padding: 20px;\n  width: 79.1%;\n  margin-left: 10%;\n  margin-bottom: 3%;\n  background-color: #ff9800;\n  color: white;\n  z-index: 1;\n}\n\n#alert-inv-2.show {\n  visibility: visible;\n}\n\n.dark-theme > .modal-content {\n  background-color: #333;\n  color: #ecf0f1;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXV0aC9hdXRoLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLGFBQWE7RUFDYixxQkFBcUI7RUFDckIsc0JBQXNCO0VBQ3RCLHNCQUFzQjtFQUN0Qix5QkFBeUI7QUFDM0I7O0FBRUE7RUFDRSxzQkFBc0I7RUFDdEIsYUFBYTtBQUNmOztBQUVBO0VBQ0UseUJBQXlCO0VBQ3pCLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsYUFBYTtFQUNiLHVCQUF1QjtFQUN2QixZQUFZO0VBQ1osZUFBZTtFQUNmLFdBQVc7QUFDYjs7QUFFQTtFQUNFLFlBQVk7QUFDZDs7QUFFQTtBQUNBLFdBQVc7QUFDWCxrQkFBa0I7QUFDbEIseUJBQXlCO0FBQ3pCOztBQUVBO0VBQ0UsYUFBYTtBQUNmOztBQUVBO0VBQ0UsWUFBWTtFQUNaLGlCQUFpQjtBQUNuQjs7QUFFQTtFQUNFLGFBQWE7RUFDYixlQUFlO0VBQ2YsVUFBVTtFQUNWLE9BQU87RUFDUCxNQUFNO0VBQ04sV0FBVztFQUNYLFlBQVk7RUFDWixjQUFjO0VBQ2QsNEJBQTRCO0VBQzVCLGlDQUFpQztFQUNqQyxpQkFBaUI7QUFDbkI7O0FBRUE7RUFDRSx5QkFBeUI7RUFDekIsd0JBQXdCO0VBQ3hCLHNCQUFzQjtFQUN0QixVQUFVO0FBQ1o7O0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsV0FBVztFQUNYLFFBQVE7RUFDUixXQUFXO0VBQ1gsZUFBZTtFQUNmLGlCQUFpQjtBQUNuQjs7QUFFQTs7RUFFRSxVQUFVO0VBQ1YsZUFBZTtBQUNqQjs7QUFFQTtFQUNFLG1DQUFtQztFQUNuQztBQUNGOztBQUVBO0VBQ0UsTUFBTSwyQkFBMkI7RUFDakMsSUFBSSwyQkFBMkI7QUFDakM7O0FBRUE7RUFDRSxNQUFNLG1CQUFtQjtFQUN6QixJQUFJLG1CQUFtQjtBQUN6Qjs7QUFFQTtFQUNFO0tBQ0csY0FBYztLQUNkLFdBQVc7RUFDZDtFQUNBO0tBQ0csV0FBVztFQUNkO0FBQ0Y7O0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsYUFBYTtFQUNiLFlBQVk7RUFDWixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLHlCQUF5QjtFQUN6QixZQUFZO0VBQ1osVUFBVTtBQUNaOztBQUVBO0VBQ0UsbUJBQW1CO0FBQ3JCOztBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLGFBQWE7RUFDYixZQUFZO0VBQ1osZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQix5QkFBeUI7RUFDekIsWUFBWTtFQUNaLFVBQVU7QUFDWjs7QUFFQTtFQUNFLG1CQUFtQjtBQUNyQjs7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixhQUFhO0VBQ2IsWUFBWTtFQUNaLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIseUJBQXlCO0VBQ3pCLFlBQVk7RUFDWixVQUFVO0FBQ1o7O0FBRUE7RUFDRSxtQkFBbUI7QUFDckI7O0FBRUE7RUFDRSxNQUFNLFNBQVMsRUFBRSxVQUFVLENBQUM7RUFDNUIsSUFBSSxZQUFZLEVBQUUsVUFBVSxDQUFDO0FBQy9COztBQUVBO0VBQ0UsTUFBTSxTQUFTLEVBQUUsVUFBVSxDQUFDO0VBQzVCLElBQUksWUFBWSxFQUFFLFVBQVUsQ0FBQztBQUMvQjs7QUFFQTtFQUNFLE1BQU0sWUFBWSxFQUFFLFVBQVUsQ0FBQztFQUMvQixJQUFJLFNBQVMsRUFBRSxVQUFVLENBQUM7QUFDNUI7O0FBRUE7RUFDRSxNQUFNLFlBQVksRUFBRSxVQUFVLENBQUM7RUFDL0IsSUFBSSxTQUFTLEVBQUUsVUFBVSxDQUFDO0FBQzVCOztBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLGFBQWE7RUFDYixZQUFZO0VBQ1osZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQix5QkFBeUI7RUFDekIsWUFBWTtFQUNaLFVBQVU7QUFDWjs7QUFFQTtFQUNFLG1CQUFtQjtBQUNyQjs7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixhQUFhO0VBQ2IsWUFBWTtFQUNaLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIseUJBQXlCO0VBQ3pCLFlBQVk7RUFDWixVQUFVO0FBQ1o7O0FBRUE7RUFDRSxtQkFBbUI7QUFDckI7O0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsYUFBYTtFQUNiLFlBQVk7RUFDWixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLHlCQUF5QjtFQUN6QixZQUFZO0VBQ1osVUFBVTtBQUNaOztBQUVBO0VBQ0UsbUJBQW1CO0FBQ3JCOztBQUVBO0VBQ0Usc0JBQXNCO0VBQ3RCLGNBQWM7QUFDaEIiLCJmaWxlIjoic3JjL2FwcC9hdXRoL2F1dGguY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbImlucHV0W3R5cGU9dGV4dF0sIGlucHV0W3R5cGU9cGFzc3dvcmRdIHtcbiAgd2lkdGg6IDEwMCU7XG4gIHBhZGRpbmc6IDEycHggMjBweDtcbiAgbWFyZ2luOiA4cHggMDtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjFmMWYxO1xufVxuXG5pbnB1dFt0eXBlPXRleHRdOmZvY3VzLCBpbnB1dFt0eXBlPXBhc3N3b3JkXTpmb2N1cyB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNkZGQ7XG4gIG91dGxpbmU6IG5vbmU7XG59XG4gIFxuYnV0dG9uIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzIxOTllODtcbiAgY29sb3I6IHdoaXRlO1xuICBwYWRkaW5nOiAxNHB4IDIwcHg7XG4gIG1hcmdpbjogOHB4IDA7XG4gIG1hcmdpbi1yaWdodDogMS4wNjI1cmVtO1xuICBib3JkZXI6IG5vbmU7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbmJ1dHRvbjpob3ZlciB7XG4gIG9wYWNpdHk6IDAuODtcbn1cblxuLmNhbmNlbGJ0biB7XG53aWR0aDogYXV0bztcbnBhZGRpbmc6IDEwcHggMThweDtcbmJhY2tncm91bmQtY29sb3I6ICNmNDQzMzY7XG59XG5cbi5jb250YWluZXIge1xuICBwYWRkaW5nOiAxNnB4O1xufVxuXG5zcGFuLnBzdyB7XG4gIGZsb2F0OiByaWdodDtcbiAgcGFkZGluZy10b3A6IDE2cHg7XG59XG4gIFxuLm1vZGFsIHtcbiAgZGlzcGxheTogbm9uZTtcbiAgcG9zaXRpb246IGZpeGVkO1xuICB6LWluZGV4OiAxO1xuICBsZWZ0OiAwO1xuICB0b3A6IDA7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIG92ZXJmbG93OiBhdXRvO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMCwwLDApO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsMCwwLDAuNCk7XG4gIHBhZGRpbmctdG9wOiA2MHB4O1xufVxuXG4ubW9kYWwtY29udGVudCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZWZlZmU7XG4gIG1hcmdpbjogNSUgYXV0byAxNSUgYXV0bztcbiAgYm9yZGVyOiAxcHggc29saWQgIzg4ODtcbiAgd2lkdGg6IDgwJTtcbn1cblxuLmNsb3NlIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogMjVweDtcbiAgdG9wOiA4cHg7XG4gIGNvbG9yOiAjMDAwO1xuICBmb250LXNpemU6IDM1cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4uY2xvc2U6aG92ZXIsXG4uY2xvc2U6Zm9jdXMge1xuICBjb2xvcjogcmVkO1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG5cbi5hbmltYXRlIHtcbiAgLXdlYmtpdC1hbmltYXRpb246IGFuaW1hdGV6b29tIDAuNnM7XG4gIGFuaW1hdGlvbjogYW5pbWF0ZXpvb20gMC42c1xufVxuICBcbkAtd2Via2l0LWtleWZyYW1lcyBhbmltYXRlem9vbSB7XG4gIGZyb20gey13ZWJraXQtdHJhbnNmb3JtOiBzY2FsZSgwKX0gXG4gIHRvIHstd2Via2l0LXRyYW5zZm9ybTogc2NhbGUoMSl9XG59XG4gIFxuQGtleWZyYW1lcyBhbmltYXRlem9vbSB7XG4gIGZyb20ge3RyYW5zZm9ybTogc2NhbGUoMCl9IFxuICB0byB7dHJhbnNmb3JtOiBzY2FsZSgxKX1cbn1cbiAgXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAzMDBweCkge1xuICBzcGFuLnBzdyB7XG4gICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICBmbG9hdDogbm9uZTtcbiAgfVxuICAuY2FuY2VsYnRuIHtcbiAgICAgd2lkdGg6IDEwMCU7XG4gIH1cbn1cblxuI2FsZXJ0LWJhZCB7XG4gIHZpc2liaWxpdHk6IGhpZGRlbjtcbiAgcGFkZGluZzogMjBweDtcbiAgd2lkdGg6IDc5LjElO1xuICBtYXJnaW4tbGVmdDogMTAlO1xuICBtYXJnaW4tYm90dG9tOiAzJTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y0NDMzNjtcbiAgY29sb3I6IHdoaXRlO1xuICB6LWluZGV4OiAxO1xufVxuXG4jYWxlcnQtYmFkLnNob3cge1xuICB2aXNpYmlsaXR5OiB2aXNpYmxlO1xufVxuXG4jYWxlcnQtZ29vZCB7XG4gIHZpc2liaWxpdHk6IGhpZGRlbjtcbiAgcGFkZGluZzogMjBweDtcbiAgd2lkdGg6IDc5LjElO1xuICBtYXJnaW4tbGVmdDogMTAlO1xuICBtYXJnaW4tYm90dG9tOiAzJTtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzRDQUY1MDtcbiAgY29sb3I6IHdoaXRlO1xuICB6LWluZGV4OiAxO1xufVxuXG4jYWxlcnQtZ29vZC5zaG93IHtcbiAgdmlzaWJpbGl0eTogdmlzaWJsZTtcbn1cbiAgXG4jYWxlcnQtaW52IHtcbiAgdmlzaWJpbGl0eTogaGlkZGVuO1xuICBwYWRkaW5nOiAyMHB4O1xuICB3aWR0aDogNzkuMSU7XG4gIG1hcmdpbi1sZWZ0OiAxMCU7XG4gIG1hcmdpbi1ib3R0b206IDMlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmY5ODAwO1xuICBjb2xvcjogd2hpdGU7XG4gIHotaW5kZXg6IDE7XG59XG5cbiNhbGVydC1pbnYuc2hvdyB7XG4gIHZpc2liaWxpdHk6IHZpc2libGU7XG59XG4gIFxuQC13ZWJraXQta2V5ZnJhbWVzIGZhZGVpbiB7XG4gIGZyb20ge2JvdHRvbTogMDsgb3BhY2l0eTogMDt9IFxuICB0byB7Ym90dG9tOiAzMHB4OyBvcGFjaXR5OiAxO31cbn1cbiAgXG5Aa2V5ZnJhbWVzIGZhZGVpbiB7XG4gIGZyb20ge2JvdHRvbTogMDsgb3BhY2l0eTogMDt9XG4gIHRvIHtib3R0b206IDMwcHg7IG9wYWNpdHk6IDE7fVxufVxuICBcbkAtd2Via2l0LWtleWZyYW1lcyBmYWRlb3V0IHtcbiAgZnJvbSB7Ym90dG9tOiAzMHB4OyBvcGFjaXR5OiAxO30gXG4gIHRvIHtib3R0b206IDA7IG9wYWNpdHk6IDA7fVxufVxuICBcbkBrZXlmcmFtZXMgZmFkZW91dCB7XG4gIGZyb20ge2JvdHRvbTogMzBweDsgb3BhY2l0eTogMTt9XG4gIHRvIHtib3R0b206IDA7IG9wYWNpdHk6IDA7fVxufVxuXG4jYWxlcnQtYmFkLTIge1xuICB2aXNpYmlsaXR5OiBoaWRkZW47XG4gIHBhZGRpbmc6IDIwcHg7XG4gIHdpZHRoOiA3OS4xJTtcbiAgbWFyZ2luLWxlZnQ6IDEwJTtcbiAgbWFyZ2luLWJvdHRvbTogMyU7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmNDQzMzY7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgei1pbmRleDogMTtcbn1cblxuI2FsZXJ0LWJhZC0yLnNob3cge1xuICB2aXNpYmlsaXR5OiB2aXNpYmxlO1xufVxuXG4jYWxlcnQtZ29vZC0yIHtcbiAgdmlzaWJpbGl0eTogaGlkZGVuO1xuICBwYWRkaW5nOiAyMHB4O1xuICB3aWR0aDogNzkuMSU7XG4gIG1hcmdpbi1sZWZ0OiAxMCU7XG4gIG1hcmdpbi1ib3R0b206IDMlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNENBRjUwO1xuICBjb2xvcjogd2hpdGU7XG4gIHotaW5kZXg6IDE7XG59XG5cbiNhbGVydC1nb29kLTIuc2hvdyB7XG4gIHZpc2liaWxpdHk6IHZpc2libGU7XG59XG4gIFxuI2FsZXJ0LWludi0yIHtcbiAgdmlzaWJpbGl0eTogaGlkZGVuO1xuICBwYWRkaW5nOiAyMHB4O1xuICB3aWR0aDogNzkuMSU7XG4gIG1hcmdpbi1sZWZ0OiAxMCU7XG4gIG1hcmdpbi1ib3R0b206IDMlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmY5ODAwO1xuICBjb2xvcjogd2hpdGU7XG4gIHotaW5kZXg6IDE7XG59XG5cbiNhbGVydC1pbnYtMi5zaG93IHtcbiAgdmlzaWJpbGl0eTogdmlzaWJsZTtcbn1cblxuLmRhcmstdGhlbWUgPiAubW9kYWwtY29udGVudCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzMzM7XG4gIGNvbG9yOiAjZWNmMGYxO1xufSJdfQ== */");
            /***/ 
        }),
        /***/ "./src/app/auth/auth.component.ts": 
        /*!****************************************!*\
          !*** ./src/app/auth/auth.component.ts ***!
          \****************************************/
        /*! exports provided: AuthComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthComponent", function () { return AuthComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _userClass_user__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../userClass/user */ "./src/app/userClass/user.ts");
            /* harmony import */ var _services_auth_log_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/auth-log.service */ "./src/app/services/auth-log.service.ts");
            var AuthComponent = /** @class */ (function () {
                function AuthComponent(authLogService) {
                    this.authLogService = authLogService;
                    this.user = new _userClass_user__WEBPACK_IMPORTED_MODULE_2__["User"]();
                }
                AuthComponent.prototype.ngOnInit = function () {
                    this.user.login = '';
                    this.user.password = '';
                };
                AuthComponent.prototype.submit = function (user) {
                    var _this = this;
                    this.done = false;
                    this.authLogService.postData(user)
                        .subscribe(function (data) {
                        _this.done = true;
                        _this.receivedUser = data;
                        localStorage.setItem('jwtToken', JSON.stringify(data.token));
                        localStorage.setItem('user', JSON.stringify(data));
                        var alert = document.getElementById('alert-good');
                        alert.className = 'show';
                        var modal = document.getElementById('id01');
                        setTimeout(function () {
                            alert.className = alert.className.replace('show', '');
                        }, 1500);
                        setTimeout(function () {
                            modal.style.display = 'none';
                        }, 1000);
                    }, function (error) {
                        var alert = document.getElementById('alert-inv');
                        alert.className = 'show';
                        setTimeout(function () {
                            alert.className = alert.className.replace('show', '');
                        }, 3000);
                    });
                };
                AuthComponent.prototype.badInfo = function () {
                    var alert = document.getElementById('alert-bad');
                    alert.className = 'show';
                    setTimeout(function () {
                        alert.className = alert.className.replace('show', '');
                    }, 3000);
                };
                AuthComponent.prototype.clearInput = function () {
                    this.user.login = '';
                    this.user.password = '';
                };
                return AuthComponent;
            }());
            AuthComponent.ctorParameters = function () { return [
                { type: _services_auth_log_service__WEBPACK_IMPORTED_MODULE_3__["AuthLogService"] }
            ]; };
            AuthComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-auth',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./auth.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/auth/auth.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./auth.component.css */ "./src/app/auth/auth.component.css")).default]
                })
            ], AuthComponent);
            /***/ 
        }),
        /***/ "./src/app/logout/logout.component.css": 
        /*!*********************************************!*\
          !*** ./src/app/logout/logout.component.css ***!
          \*********************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("button {\n    background-color: #2199e8;\n    color: white;\n    padding: 14px 20px;\n    margin: 8px 0;\n    margin-right: 1.0625rem;\n    border: none;\n    cursor: pointer;\n    width: 100%;\n}\n  \nbutton:hover {\n    opacity: 0.8;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9nb3V0L2xvZ291dC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0kseUJBQXlCO0lBQ3pCLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsYUFBYTtJQUNiLHVCQUF1QjtJQUN2QixZQUFZO0lBQ1osZUFBZTtJQUNmLFdBQVc7QUFDZjs7QUFFQTtJQUNJLFlBQVk7QUFDaEIiLCJmaWxlIjoic3JjL2FwcC9sb2dvdXQvbG9nb3V0LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJidXR0b24ge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMyMTk5ZTg7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIHBhZGRpbmc6IDE0cHggMjBweDtcbiAgICBtYXJnaW46IDhweCAwO1xuICAgIG1hcmdpbi1yaWdodDogMS4wNjI1cmVtO1xuICAgIGJvcmRlcjogbm9uZTtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgd2lkdGg6IDEwMCU7XG59XG4gIFxuYnV0dG9uOmhvdmVyIHtcbiAgICBvcGFjaXR5OiAwLjg7XG59Il19 */");
            /***/ 
        }),
        /***/ "./src/app/logout/logout.component.ts": 
        /*!********************************************!*\
          !*** ./src/app/logout/logout.component.ts ***!
          \********************************************/
        /*! exports provided: LogoutComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogoutComponent", function () { return LogoutComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _services_auth_log_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/auth-log.service */ "./src/app/services/auth-log.service.ts");
            var LogoutComponent = /** @class */ (function () {
                function LogoutComponent(authLogService) {
                    this.authLogService = authLogService;
                }
                LogoutComponent.prototype.ngOnInit = function () {
                    this.token = localStorage.getItem('jwtToken');
                };
                LogoutComponent.prototype.logout = function () {
                    var _this = this;
                    this.token = null;
                    localStorage.removeItem('jwtToken');
                    localStorage.removeItem('user');
                    this.authLogService.logout().subscribe(function (data) {
                        _this.user = data;
                    });
                };
                LogoutComponent.prototype.userInfo = function () {
                    if (this.token != null) {
                        this.currentUser = JSON.parse(localStorage.getItem('user'));
                    }
                };
                return LogoutComponent;
            }());
            LogoutComponent.ctorParameters = function () { return [
                { type: _services_auth_log_service__WEBPACK_IMPORTED_MODULE_2__["AuthLogService"] }
            ]; };
            LogoutComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-logout',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./logout.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/logout/logout.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./logout.component.css */ "./src/app/logout/logout.component.css")).default]
                })
            ], LogoutComponent);
            /***/ 
        }),
        /***/ "./src/app/registration/registration.component.css": 
        /*!*********************************************************!*\
          !*** ./src/app/registration/registration.component.css ***!
          \*********************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("input[type=text], input[type=password], input[type=email] {\n  width: 100%;\n  padding: 12px 20px;\n  margin: 8px 0;\n  display: inline-block;\n  border: 1px solid #ccc;\n  box-sizing: border-box;\n  background-color: #f1f1f1;\n}\n\ninput[type=text]:focus, input[type=password]:focus, input[type=email]:focus {\n  background-color: #ddd;\n  outline: none;\n}\n\nbutton {\n  background-color: #2199e8;\n  color: white;\n  padding: 14px 20px;\n  margin: 8px 0;\n  margin-right: 1.0625rem;\n  border: none;\n  cursor: pointer;\n  width: 100%;\n}\n\nbutton:hover {\n  opacity: 0.8;\n}\n\n.cancelbtn {\n  padding: 10px 18px;\n  background-color: #f44336;\n}\n\n.signupbtn {\n  float: left;\n  width: 100%;\n}\n\n.container {\n  padding: 16px;\n}\n\n.modal {\n  display: none;\n  position: fixed;\n  z-index: 1;\n  left: 0;\n  top: 0;\n  width: 100%;\n  height: 100%;\n  overflow: auto;\n  background-color: rgba(0,0,0,0.4);\n  padding-top: 50px;\n}\n\n.modal-content {\n  background-color: #fefefe;\n  margin: 5% auto 15% auto;\n  border: 1px solid #888;\n  width: 80%;\n}\n\n.close {\n  position: absolute;\n  right: 25px;\n  top: 8px;\n  color: #000;\n  font-size: 35px;\n  font-weight: bold;\n}\n\n.close:hover,\n.close:focus {\n  color: red;\n  cursor: pointer;\n}\n\nhr {\n  border: 1px solid #f1f1f1;\n  margin-bottom: 25px;\n}\n\n.clearfix::after {\n  content: \"\";\n  clear: both;\n  display: table;\n}\n\n.animate {\n  -webkit-animation: animatezoom 0.6s;\n  animation: animatezoom 0.6s\n}\n\n@-webkit-keyframes animatezoom {\n  from {-webkit-transform: scale(0)} \n  to {-webkit-transform: scale(1)}\n}\n\n@keyframes animatezoom {\n  from {transform: scale(0)} \n  to {transform: scale(1)}\n}\n\n@media screen and (max-width: 300px) {\n  .cancelbtn, .signupbtn {\n     width: 100%;\n  }\n}\n\n#alert-bad-reg {\n  visibility: hidden;\n  padding: 20px;\n  width: 79.1%;\n  margin-left: 10%;\n  margin-bottom: 3%;\n  background-color: #f44336;\n  color: white;\n  z-index: 1;\n}\n\n#alert-bad-reg.show {\n  visibility: visible;\n}\n\n#alert-good-reg {\n  visibility: hidden;\n  padding: 20px;\n  width: 79.1%;\n  margin-left: 10%;\n  margin-bottom: 3%;\n  background-color: #4CAF50;\n  color: white;\n  z-index: 1;\n}\n\n#alert-good-reg.show {\n  visibility: visible;\n}\n\n.button-block {\n  display: flex;\n  flex-direction: row-reverse;\n}\n\n#alert-inv-reg {\n  visibility: hidden;\n  padding: 20px;\n  width: 79.1%;\n  margin-left: 10%;\n  margin-bottom: 3%;\n  background-color: #ff9800;\n  color: white;\n  z-index: 1;\n}\n\n#alert-inv-reg.show {\n  visibility: visible;\n}\n\n.file-upload {\n  margin-left: 11%;\n}\n\n.dark-theme > .modal-content {\n  background-color: #333;\n  color: #ecf0f1;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVnaXN0cmF0aW9uL3JlZ2lzdHJhdGlvbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixhQUFhO0VBQ2IscUJBQXFCO0VBQ3JCLHNCQUFzQjtFQUN0QixzQkFBc0I7RUFDdEIseUJBQXlCO0FBQzNCOztBQUVBO0VBQ0Usc0JBQXNCO0VBQ3RCLGFBQWE7QUFDZjs7QUFFQTtFQUNFLHlCQUF5QjtFQUN6QixZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGFBQWE7RUFDYix1QkFBdUI7RUFDdkIsWUFBWTtFQUNaLGVBQWU7RUFDZixXQUFXO0FBQ2I7O0FBRUE7RUFDRSxZQUFZO0FBQ2Q7O0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIseUJBQXlCO0FBQzNCOztBQUVBO0VBQ0UsV0FBVztFQUNYLFdBQVc7QUFDYjs7QUFFQTtFQUNFLGFBQWE7QUFDZjs7QUFFQTtFQUNFLGFBQWE7RUFDYixlQUFlO0VBQ2YsVUFBVTtFQUNWLE9BQU87RUFDUCxNQUFNO0VBQ04sV0FBVztFQUNYLFlBQVk7RUFDWixjQUFjO0VBQ2QsaUNBQWlDO0VBQ2pDLGlCQUFpQjtBQUNuQjs7QUFFQTtFQUNFLHlCQUF5QjtFQUN6Qix3QkFBd0I7RUFDeEIsc0JBQXNCO0VBQ3RCLFVBQVU7QUFDWjs7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsUUFBUTtFQUNSLFdBQVc7RUFDWCxlQUFlO0VBQ2YsaUJBQWlCO0FBQ25COztBQUVBOztFQUVFLFVBQVU7RUFDVixlQUFlO0FBQ2pCOztBQUVBO0VBQ0UseUJBQXlCO0VBQ3pCLG1CQUFtQjtBQUNyQjs7QUFFQTtFQUNFLFdBQVc7RUFDWCxXQUFXO0VBQ1gsY0FBYztBQUNoQjs7QUFFQTtFQUNFLG1DQUFtQztFQUNuQztBQUNGOztBQUVBO0VBQ0UsTUFBTSwyQkFBMkI7RUFDakMsSUFBSSwyQkFBMkI7QUFDakM7O0FBRUE7RUFDRSxNQUFNLG1CQUFtQjtFQUN6QixJQUFJLG1CQUFtQjtBQUN6Qjs7QUFFQTtFQUNFO0tBQ0csV0FBVztFQUNkO0FBQ0Y7O0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsYUFBYTtFQUNiLFlBQVk7RUFDWixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLHlCQUF5QjtFQUN6QixZQUFZO0VBQ1osVUFBVTtBQUNaOztBQUVBO0VBQ0UsbUJBQW1CO0FBQ3JCOztBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLGFBQWE7RUFDYixZQUFZO0VBQ1osZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQix5QkFBeUI7RUFDekIsWUFBWTtFQUNaLFVBQVU7QUFDWjs7QUFFQTtFQUNFLG1CQUFtQjtBQUNyQjs7QUFFQTtFQUNFLGFBQWE7RUFDYiwyQkFBMkI7QUFDN0I7O0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsYUFBYTtFQUNiLFlBQVk7RUFDWixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLHlCQUF5QjtFQUN6QixZQUFZO0VBQ1osVUFBVTtBQUNaOztBQUVBO0VBQ0UsbUJBQW1CO0FBQ3JCOztBQUVBO0VBQ0UsZ0JBQWdCO0FBQ2xCOztBQUVBO0VBQ0Usc0JBQXNCO0VBQ3RCLGNBQWM7QUFDaEIiLCJmaWxlIjoic3JjL2FwcC9yZWdpc3RyYXRpb24vcmVnaXN0cmF0aW9uLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpbnB1dFt0eXBlPXRleHRdLCBpbnB1dFt0eXBlPXBhc3N3b3JkXSwgaW5wdXRbdHlwZT1lbWFpbF0ge1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogMTJweCAyMHB4O1xuICBtYXJnaW46IDhweCAwO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmMWYxZjE7XG59XG5cbmlucHV0W3R5cGU9dGV4dF06Zm9jdXMsIGlucHV0W3R5cGU9cGFzc3dvcmRdOmZvY3VzLCBpbnB1dFt0eXBlPWVtYWlsXTpmb2N1cyB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNkZGQ7XG4gIG91dGxpbmU6IG5vbmU7XG59XG5cbmJ1dHRvbiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMyMTk5ZTg7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgcGFkZGluZzogMTRweCAyMHB4O1xuICBtYXJnaW46IDhweCAwO1xuICBtYXJnaW4tcmlnaHQ6IDEuMDYyNXJlbTtcbiAgYm9yZGVyOiBub25lO1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG5idXR0b246aG92ZXIge1xuICBvcGFjaXR5OiAwLjg7XG59XG5cbi5jYW5jZWxidG4ge1xuICBwYWRkaW5nOiAxMHB4IDE4cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmNDQzMzY7XG59XG5cbi5zaWdudXBidG4ge1xuICBmbG9hdDogbGVmdDtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5jb250YWluZXIge1xuICBwYWRkaW5nOiAxNnB4O1xufVxuXG4ubW9kYWwge1xuICBkaXNwbGF5OiBub25lO1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIHotaW5kZXg6IDE7XG4gIGxlZnQ6IDA7XG4gIHRvcDogMDtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgb3ZlcmZsb3c6IGF1dG87XG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsMC40KTtcbiAgcGFkZGluZy10b3A6IDUwcHg7XG59XG5cbi5tb2RhbC1jb250ZW50IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZlZmVmZTtcbiAgbWFyZ2luOiA1JSBhdXRvIDE1JSBhdXRvO1xuICBib3JkZXI6IDFweCBzb2xpZCAjODg4O1xuICB3aWR0aDogODAlO1xufVxuXG4uY2xvc2Uge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiAyNXB4O1xuICB0b3A6IDhweDtcbiAgY29sb3I6ICMwMDA7XG4gIGZvbnQtc2l6ZTogMzVweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbi5jbG9zZTpob3Zlcixcbi5jbG9zZTpmb2N1cyB7XG4gIGNvbG9yOiByZWQ7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbiAgXG5ociB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNmMWYxZjE7XG4gIG1hcmdpbi1ib3R0b206IDI1cHg7XG59XG5cbi5jbGVhcmZpeDo6YWZ0ZXIge1xuICBjb250ZW50OiBcIlwiO1xuICBjbGVhcjogYm90aDtcbiAgZGlzcGxheTogdGFibGU7XG59XG5cbi5hbmltYXRlIHtcbiAgLXdlYmtpdC1hbmltYXRpb246IGFuaW1hdGV6b29tIDAuNnM7XG4gIGFuaW1hdGlvbjogYW5pbWF0ZXpvb20gMC42c1xufVxuXG5ALXdlYmtpdC1rZXlmcmFtZXMgYW5pbWF0ZXpvb20ge1xuICBmcm9tIHstd2Via2l0LXRyYW5zZm9ybTogc2NhbGUoMCl9IFxuICB0byB7LXdlYmtpdC10cmFuc2Zvcm06IHNjYWxlKDEpfVxufVxuICAgIFxuQGtleWZyYW1lcyBhbmltYXRlem9vbSB7XG4gIGZyb20ge3RyYW5zZm9ybTogc2NhbGUoMCl9IFxuICB0byB7dHJhbnNmb3JtOiBzY2FsZSgxKX1cbn1cblxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogMzAwcHgpIHtcbiAgLmNhbmNlbGJ0biwgLnNpZ251cGJ0biB7XG4gICAgIHdpZHRoOiAxMDAlO1xuICB9XG59XG4gIFxuI2FsZXJ0LWJhZC1yZWcge1xuICB2aXNpYmlsaXR5OiBoaWRkZW47XG4gIHBhZGRpbmc6IDIwcHg7XG4gIHdpZHRoOiA3OS4xJTtcbiAgbWFyZ2luLWxlZnQ6IDEwJTtcbiAgbWFyZ2luLWJvdHRvbTogMyU7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmNDQzMzY7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgei1pbmRleDogMTtcbn1cbiAgXG4jYWxlcnQtYmFkLXJlZy5zaG93IHtcbiAgdmlzaWJpbGl0eTogdmlzaWJsZTtcbn1cblxuI2FsZXJ0LWdvb2QtcmVnIHtcbiAgdmlzaWJpbGl0eTogaGlkZGVuO1xuICBwYWRkaW5nOiAyMHB4O1xuICB3aWR0aDogNzkuMSU7XG4gIG1hcmdpbi1sZWZ0OiAxMCU7XG4gIG1hcmdpbi1ib3R0b206IDMlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNENBRjUwO1xuICBjb2xvcjogd2hpdGU7XG4gIHotaW5kZXg6IDE7XG59XG5cbiNhbGVydC1nb29kLXJlZy5zaG93IHtcbiAgdmlzaWJpbGl0eTogdmlzaWJsZTtcbn1cblxuLmJ1dHRvbi1ibG9jayB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3ctcmV2ZXJzZTtcbn1cbiAgXG4jYWxlcnQtaW52LXJlZyB7XG4gIHZpc2liaWxpdHk6IGhpZGRlbjtcbiAgcGFkZGluZzogMjBweDtcbiAgd2lkdGg6IDc5LjElO1xuICBtYXJnaW4tbGVmdDogMTAlO1xuICBtYXJnaW4tYm90dG9tOiAzJTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmOTgwMDtcbiAgY29sb3I6IHdoaXRlO1xuICB6LWluZGV4OiAxO1xufVxuICBcbiNhbGVydC1pbnYtcmVnLnNob3cge1xuICB2aXNpYmlsaXR5OiB2aXNpYmxlO1xufVxuICBcbi5maWxlLXVwbG9hZCB7XG4gIG1hcmdpbi1sZWZ0OiAxMSU7XG59XG5cbi5kYXJrLXRoZW1lID4gLm1vZGFsLWNvbnRlbnQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzMzO1xuICBjb2xvcjogI2VjZjBmMTtcbn0iXX0= */");
            /***/ 
        }),
        /***/ "./src/app/registration/registration.component.ts": 
        /*!********************************************************!*\
          !*** ./src/app/registration/registration.component.ts ***!
          \********************************************************/
        /*! exports provided: RegistrationComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistrationComponent", function () { return RegistrationComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _userClass_user__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../userClass/user */ "./src/app/userClass/user.ts");
            /* harmony import */ var _services_auth_reg_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/auth-reg.service */ "./src/app/services/auth-reg.service.ts");
            var RegistrationComponent = /** @class */ (function () {
                function RegistrationComponent(authRegService) {
                    this.authRegService = authRegService;
                    this.user = new _userClass_user__WEBPACK_IMPORTED_MODULE_2__["User"]();
                    this.done = false;
                    this.errorBoolean = false;
                }
                RegistrationComponent.prototype.ngOnInit = function () {
                    this.user.login = '';
                    this.user.password = '';
                    this.errorBoolean = false;
                };
                RegistrationComponent.prototype.submit = function (user) {
                    var _this = this;
                    this.errorBoolean = false;
                    if (user.login.trim().length === 0 || user.password.trim().length === 0) {
                        this.badInfo();
                        this.errorBoolean = true;
                    }
                    else {
                        this.authRegService.postData(user)
                            .subscribe(function (data) {
                            _this.receivedUser = data;
                            _this.done = true;
                            localStorage.setItem('jwtToken', JSON.stringify(data.token));
                            localStorage.setItem('user', JSON.stringify(data));
                            var alert = document.getElementById('alert-good-reg');
                            alert.className = 'show';
                            var modal = document.getElementById('id02');
                            setTimeout(function () { alert.className = alert.className.replace('show', ''); }, 1500);
                            setTimeout(function () {
                                modal.style.display = 'none';
                            }, 1000);
                        }, function (error) {
                            if (_this.errorBoolean === false) {
                                _this.invInfo();
                            }
                        });
                    }
                };
                RegistrationComponent.prototype.badInfo = function () {
                    var alert = document.getElementById('alert-bad-reg');
                    alert.className = 'show';
                    setTimeout(function () { alert.className = alert.className.replace('show', ''); }, 3000);
                };
                RegistrationComponent.prototype.goodInfo = function () {
                    var alert = document.getElementById('alert-good-reg');
                    alert.className = 'show';
                    var modal = document.getElementById('id02');
                    setTimeout(function () { alert.className = alert.className.replace('show', ''); }, 1500);
                    setTimeout(function () { modal.style.display = 'none'; }, 1500);
                };
                RegistrationComponent.prototype.invInfo = function () {
                    var alert = document.getElementById('alert-inv-reg');
                    alert.className = 'show';
                    setTimeout(function () { alert.className = alert.className.replace('show', ''); }, 3000);
                };
                RegistrationComponent.prototype.clearInput = function () {
                    this.user.login = '';
                    this.user.password = '';
                };
                return RegistrationComponent;
            }());
            RegistrationComponent.ctorParameters = function () { return [
                { type: _services_auth_reg_service__WEBPACK_IMPORTED_MODULE_3__["AuthRegService"] }
            ]; };
            RegistrationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-registration',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./registration.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/registration/registration.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./registration.component.css */ "./src/app/registration/registration.component.css")).default]
                })
            ], RegistrationComponent);
            /***/ 
        }),
        /***/ "./src/app/services/auth-log.service.ts": 
        /*!**********************************************!*\
          !*** ./src/app/services/auth-log.service.ts ***!
          \**********************************************/
        /*! exports provided: AuthLogService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthLogService", function () { return AuthLogService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            var AuthLogService = /** @class */ (function () {
                function AuthLogService(http) {
                    this.http = http;
                }
                AuthLogService.prototype.postData = function (user) {
                    var body = { login: user.login, password: user.password };
                    return this.http.post('http://192.168.1.99:3000/auth/signin', body);
                };
                AuthLogService.prototype.logout = function () {
                    return this.http.get('http://192.168.1.99:3000/auth/logout', { withCredentials: true });
                };
                return AuthLogService;
            }());
            AuthLogService.ctorParameters = function () { return [
                { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
            ]; };
            AuthLogService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
                    providedIn: 'root'
                })
            ], AuthLogService);
            /***/ 
        }),
        /***/ "./src/app/services/auth-reg.service.ts": 
        /*!**********************************************!*\
          !*** ./src/app/services/auth-reg.service.ts ***!
          \**********************************************/
        /*! exports provided: AuthRegService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthRegService", function () { return AuthRegService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            var AuthRegService = /** @class */ (function () {
                function AuthRegService(http) {
                    this.http = http;
                }
                AuthRegService.prototype.postData = function (user) {
                    var body = { login: user.login, password: user.password };
                    return this.http.post('http://192.168.1.99:3000/auth/signup', body);
                };
                return AuthRegService;
            }());
            AuthRegService.ctorParameters = function () { return [
                { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
            ]; };
            AuthRegService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
                    providedIn: 'root'
                })
            ], AuthRegService);
            /***/ 
        }),
        /***/ "./src/app/services/chat.service.ts": 
        /*!******************************************!*\
          !*** ./src/app/services/chat.service.ts ***!
          \******************************************/
        /*! exports provided: ChatService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatService", function () { return ChatService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var socket_io_client__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! socket.io-client */ "./node_modules/socket.io-client/lib/index.js");
            /* harmony import */ var socket_io_client__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/ __webpack_require__.n(socket_io_client__WEBPACK_IMPORTED_MODULE_2__);
            /* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
            var ChatService = /** @class */ (function () {
                function ChatService() {
                    this.socket = socket_io_client__WEBPACK_IMPORTED_MODULE_2__('http://192.168.1.99:3000/');
                }
                ChatService.prototype.joinRoom = function (data) {
                    this.socket.emit('join', data);
                };
                ChatService.prototype.newUserJoined = function () {
                    var _this = this;
                    var observable = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"](function (observer) {
                        _this.socket.on('new user joined', function (data) {
                            observer.next(data);
                        });
                        return function () {
                            _this.socket.disconnect();
                        };
                    });
                    return observable;
                };
                ChatService.prototype.leaveRoom = function (data) {
                    this.socket.emit('leave', data);
                };
                ChatService.prototype.userLeftRoom = function () {
                    var _this = this;
                    var observable = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"](function (observer) {
                        _this.socket.on('left room', function (data) {
                            observer.next(data);
                        });
                        return function () {
                            _this.socket.disconnect();
                        };
                    });
                    return observable;
                };
                ChatService.prototype.sendMessage = function (data) {
                    this.socket.emit('message', data);
                };
                ChatService.prototype.newMessageReceived = function () {
                    var _this = this;
                    var observable = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"](function (observer) {
                        _this.socket.on('new message', function (data) {
                            observer.next(data);
                        });
                        return function () {
                            _this.socket.disconnect();
                        };
                    });
                    return observable;
                };
                return ChatService;
            }());
            ChatService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
                    providedIn: 'root'
                })
            ], ChatService);
            /***/ 
        }),
        /***/ "./src/app/services/online.service.ts": 
        /*!********************************************!*\
          !*** ./src/app/services/online.service.ts ***!
          \********************************************/
        /*! exports provided: OnlineService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OnlineService", function () { return OnlineService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
            /* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
            var OnlineService = /** @class */ (function () {
                function OnlineService(http) {
                    this.http = http;
                    this.inter = Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["interval"])(10000);
                    this.isUserLoggedIn$ = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"](function (observer) {
                        var currentUser = localStorage.getItem('jwtToken');
                        if (currentUser != null) {
                            observer.next(true);
                        }
                        else {
                            observer.next(false);
                        }
                    });
                }
                OnlineService.prototype.getOnlineStatus = function () {
                    return this.online$ = Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["merge"])(Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(navigator.onLine), Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["fromEvent"])(window, 'online').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["mapTo"])(true)), Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["fromEvent"])(window, 'offline').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["mapTo"])(false)));
                };
                OnlineService.prototype.longPolling = function () {
                    var _this = this;
                    return this.inter
                        .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function () {
                        return _this.http.get('http://192.168.1.99:3000/polling/connect');
                    }));
                };
                return OnlineService;
            }());
            OnlineService.ctorParameters = function () { return [
                { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
            ]; };
            OnlineService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
                    providedIn: 'root'
                })
            ], OnlineService);
            /***/ 
        }),
        /***/ "./src/app/services/random.service.ts": 
        /*!********************************************!*\
          !*** ./src/app/services/random.service.ts ***!
          \********************************************/
        /*! exports provided: RandomService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RandomService", function () { return RandomService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
            var RandomService = /** @class */ (function () {
                function RandomService() {
                    this.myNumber$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
                }
                Object.defineProperty(RandomService.prototype, "getNumber", {
                    get: function () {
                        return this.myNumber$.asObservable();
                    },
                    enumerable: true,
                    configurable: true
                });
                RandomService.prototype.newNum = function (num) {
                    this.myNumber$.next(num);
                };
                return RandomService;
            }());
            RandomService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
                    providedIn: 'root'
                })
            ], RandomService);
            /***/ 
        }),
        /***/ "./src/app/services/search.service.ts": 
        /*!********************************************!*\
          !*** ./src/app/services/search.service.ts ***!
          \********************************************/
        /*! exports provided: SearchService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchService", function () { return SearchService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
            /* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
            var SearchService = /** @class */ (function () {
                function SearchService() {
                }
                SearchService.prototype.searchMessages = function (words, keyword) {
                    if (!words) {
                        return this.search$ = Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["merge"])(Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(words)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["mapTo"])([]));
                    }
                    if (!keyword) {
                        return this.search$ = Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["merge"])(Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(words)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["mapTo"])(words));
                    }
                    return this.search$ = Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["merge"])(Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(words)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["mapTo"])(words.filter(function (item) { return item.includes(keyword); })));
                };
                return SearchService;
            }());
            SearchService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
                    providedIn: 'root'
                })
            ], SearchService);
            /***/ 
        }),
        /***/ "./src/app/task1/task1.component.css": 
        /*!*******************************************!*\
          !*** ./src/app/task1/task1.component.css ***!
          \*******************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Rhc2sxL3Rhc2sxLmNvbXBvbmVudC5jc3MifQ== */");
            /***/ 
        }),
        /***/ "./src/app/task1/task1.component.ts": 
        /*!******************************************!*\
          !*** ./src/app/task1/task1.component.ts ***!
          \******************************************/
        /*! exports provided: Task1Component */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Task1Component", function () { return Task1Component; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
            /* harmony import */ var _services_online_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/online.service */ "./src/app/services/online.service.ts");
            var Task1Component = /** @class */ (function () {
                function Task1Component(http, onlineService) {
                    this.http = http;
                    this.onlineService = onlineService;
                }
                Task1Component.prototype.ngOnInit = function () {
                    this.getOnlineStatus();
                };
                Task1Component.prototype.getOnlineStatus = function () {
                    var _this = this;
                    this.onlineService.getOnlineStatus()
                        .subscribe(function (value) {
                        _this.onlineChecker = value;
                        if (_this.onlineChecker === true) {
                            _this.longPolling();
                        }
                        else {
                            _this.stopPolling();
                        }
                    });
                };
                Task1Component.prototype.longPolling = function () {
                    this.subscription = this.onlineService.longPolling()
                        .subscribe();
                };
                Task1Component.prototype.stopPolling = function () {
                    this.subscription.unsubscribe();
                };
                return Task1Component;
            }());
            Task1Component.ctorParameters = function () { return [
                { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
                { type: _services_online_service__WEBPACK_IMPORTED_MODULE_3__["OnlineService"] }
            ]; };
            Task1Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-task1',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./task1.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/task1/task1.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./task1.component.css */ "./src/app/task1/task1.component.css")).default]
                })
            ], Task1Component);
            /***/ 
        }),
        /***/ "./src/app/task2/task2.component.css": 
        /*!*******************************************!*\
          !*** ./src/app/task2/task2.component.css ***!
          \*******************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Rhc2syL3Rhc2syLmNvbXBvbmVudC5jc3MifQ== */");
            /***/ 
        }),
        /***/ "./src/app/task2/task2.component.ts": 
        /*!******************************************!*\
          !*** ./src/app/task2/task2.component.ts ***!
          \******************************************/
        /*! exports provided: Task2Component */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Task2Component", function () { return Task2Component; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _services_online_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/online.service */ "./src/app/services/online.service.ts");
            var Task2Component = /** @class */ (function () {
                function Task2Component(onlineService) {
                    this.onlineService = onlineService;
                }
                Task2Component.prototype.ngOnInit = function () {
                    this.online();
                    this.getUser();
                };
                Task2Component.prototype.ngDoCheck = function () {
                    this.getUser();
                };
                Task2Component.prototype.online = function () {
                    var _this = this;
                    this.onlineService.getOnlineStatus()
                        .subscribe(function (value) {
                        _this.onlineChecker = value;
                    });
                };
                Task2Component.prototype.getUser = function () {
                    var _this = this;
                    this.onlineService.isUserLoggedIn$.subscribe(function (value) {
                        _this.userChecker = value;
                    });
                };
                return Task2Component;
            }());
            Task2Component.ctorParameters = function () { return [
                { type: _services_online_service__WEBPACK_IMPORTED_MODULE_2__["OnlineService"] }
            ]; };
            Task2Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-task2',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./task2.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/task2/task2.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./task2.component.css */ "./src/app/task2/task2.component.css")).default]
                })
            ], Task2Component);
            /***/ 
        }),
        /***/ "./src/app/task3/task3.component.css": 
        /*!*******************************************!*\
          !*** ./src/app/task3/task3.component.css ***!
          \*******************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".button-block-random {\n    display: flex;\n    justify-content: space-evenly;\n    width: 100%;\n    margin-left: 0.7%;\n}\n\n.values {\n    display: flex;\n    justify-content: space-evenly;\n    width: 100%;\n}\n\nbutton {\n    background-color: #2199e8;\n    color: white;\n    padding: 14px 20px;\n    margin: 8px 0;\n    margin-right: 1.0625rem;\n    border: none;\n    cursor: pointer;\n    width: 100%;\n}\n\nbutton:hover {\n    opacity: 0.8;\n}\n\n.result-array {\n    word-break: break-all;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGFzazMvdGFzazMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGFBQWE7SUFDYiw2QkFBNkI7SUFDN0IsV0FBVztJQUNYLGlCQUFpQjtBQUNyQjs7QUFFQTtJQUNJLGFBQWE7SUFDYiw2QkFBNkI7SUFDN0IsV0FBVztBQUNmOztBQUVBO0lBQ0kseUJBQXlCO0lBQ3pCLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsYUFBYTtJQUNiLHVCQUF1QjtJQUN2QixZQUFZO0lBQ1osZUFBZTtJQUNmLFdBQVc7QUFDZjs7QUFFQTtJQUNJLFlBQVk7QUFDaEI7O0FBRUE7SUFDSSxxQkFBcUI7QUFDekIiLCJmaWxlIjoic3JjL2FwcC90YXNrMy90YXNrMy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJ1dHRvbi1ibG9jay1yYW5kb20ge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1ldmVubHk7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgbWFyZ2luLWxlZnQ6IDAuNyU7XG59XG5cbi52YWx1ZXMge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1ldmVubHk7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbmJ1dHRvbiB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzIxOTllODtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgcGFkZGluZzogMTRweCAyMHB4O1xuICAgIG1hcmdpbjogOHB4IDA7XG4gICAgbWFyZ2luLXJpZ2h0OiAxLjA2MjVyZW07XG4gICAgYm9yZGVyOiBub25lO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICB3aWR0aDogMTAwJTtcbn1cbiAgXG5idXR0b246aG92ZXIge1xuICAgIG9wYWNpdHk6IDAuODtcbn1cblxuLnJlc3VsdC1hcnJheSB7XG4gICAgd29yZC1icmVhazogYnJlYWstYWxsO1xufSJdfQ== */");
            /***/ 
        }),
        /***/ "./src/app/task3/task3.component.ts": 
        /*!******************************************!*\
          !*** ./src/app/task3/task3.component.ts ***!
          \******************************************/
        /*! exports provided: Task3Component */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Task3Component", function () { return Task3Component; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _services_random_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/random.service */ "./src/app/services/random.service.ts");
            var Task3Component = /** @class */ (function () {
                function Task3Component(random) {
                    this.random = random;
                    this.randomNum = [];
                }
                Task3Component.prototype.ngOnInit = function () {
                    this.displayResult = document.querySelector('.result-array');
                };
                Task3Component.prototype.getNumber = function () {
                    var _this = this;
                    this.subscription = this.random.getNumber.subscribe(function (num) {
                        _this.randomNum.push(num);
                    });
                    this.interval = setInterval(function () {
                        _this.displayResult['textContent'] = _this.randomNum;
                    }, 2000);
                };
                Task3Component.prototype.stop = function () {
                    this.subscription.unsubscribe();
                    clearInterval(this.interval);
                };
                Task3Component.prototype.setNum = function () {
                    var _this = this;
                    this.interval = setInterval(function () {
                        _this.random.newNum(Math.floor(Math.random() * 10));
                    }, 500);
                };
                return Task3Component;
            }());
            Task3Component.ctorParameters = function () { return [
                { type: _services_random_service__WEBPACK_IMPORTED_MODULE_2__["RandomService"] }
            ]; };
            Task3Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-task3',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./task3.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/task3/task3.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./task3.component.css */ "./src/app/task3/task3.component.css")).default]
                })
            ], Task3Component);
            /***/ 
        }),
        /***/ "./src/app/task4/task4.component.css": 
        /*!*******************************************!*\
          !*** ./src/app/task4/task4.component.css ***!
          \*******************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("input[type=text], input[type=password] {\n  width: 90%;\n  padding: 12px 20px;\n  margin: 8px 5%;\n  display: inline-block;\n  border: 1px solid #ccc;\n  box-sizing: border-box;\n  background-color: #f1f1f1;\n}\n\ninput[type=text]:focus, input[type=password]:focus {\n  background-color: #ddd;\n  outline: none;\n}\n\n.words-block__item {\n  margin-bottom: .5rem;\n  padding: .5rem 1rem;\n  border: 1px dashed #ccc;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGFzazQvdGFzazQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFVBQVU7RUFDVixrQkFBa0I7RUFDbEIsY0FBYztFQUNkLHFCQUFxQjtFQUNyQixzQkFBc0I7RUFDdEIsc0JBQXNCO0VBQ3RCLHlCQUF5QjtBQUMzQjs7QUFFQTtFQUNFLHNCQUFzQjtFQUN0QixhQUFhO0FBQ2Y7O0FBRUE7RUFDRSxvQkFBb0I7RUFDcEIsbUJBQW1CO0VBQ25CLHVCQUF1QjtBQUN6QiIsImZpbGUiOiJzcmMvYXBwL3Rhc2s0L3Rhc2s0LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpbnB1dFt0eXBlPXRleHRdLCBpbnB1dFt0eXBlPXBhc3N3b3JkXSB7XG4gIHdpZHRoOiA5MCU7XG4gIHBhZGRpbmc6IDEycHggMjBweDtcbiAgbWFyZ2luOiA4cHggNSU7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YxZjFmMTtcbn1cblxuaW5wdXRbdHlwZT10ZXh0XTpmb2N1cywgaW5wdXRbdHlwZT1wYXNzd29yZF06Zm9jdXMge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZGRkO1xuICBvdXRsaW5lOiBub25lO1xufVxuXG4ud29yZHMtYmxvY2tfX2l0ZW0ge1xuICBtYXJnaW4tYm90dG9tOiAuNXJlbTtcbiAgcGFkZGluZzogLjVyZW0gMXJlbTtcbiAgYm9yZGVyOiAxcHggZGFzaGVkICNjY2M7XG59Il19 */");
            /***/ 
        }),
        /***/ "./src/app/task4/task4.component.ts": 
        /*!******************************************!*\
          !*** ./src/app/task4/task4.component.ts ***!
          \******************************************/
        /*! exports provided: Task4Component */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Task4Component", function () { return Task4Component; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _services_search_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/search.service */ "./src/app/services/search.service.ts");
            var Task4Component = /** @class */ (function () {
                function Task4Component(searchService) {
                    this.searchService = searchService;
                    this.keyword = '';
                }
                Task4Component.prototype.ngOnInit = function () {
                    this.generateWords();
                };
                Task4Component.prototype.ngDoCheck = function () {
                    this.generateWords();
                    this.searchMessages(this.words, this.keyword);
                };
                Task4Component.prototype.generateWords = function () {
                    this.words = ['word', 'owrd', 'orwd', 'ordw'];
                };
                Task4Component.prototype.searchMessages = function (words, keyword) {
                    var _this = this;
                    this.searchService.searchMessages(words, keyword)
                        .subscribe(function (value) {
                        _this.words = value;
                    });
                };
                return Task4Component;
            }());
            Task4Component.ctorParameters = function () { return [
                { type: _services_search_service__WEBPACK_IMPORTED_MODULE_2__["SearchService"] }
            ]; };
            Task4Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-task4',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./task4.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/task4/task4.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./task4.component.css */ "./src/app/task4/task4.component.css")).default]
                })
            ], Task4Component);
            /***/ 
        }),
        /***/ "./src/app/task5/task5.component.css": 
        /*!*******************************************!*\
          !*** ./src/app/task5/task5.component.css ***!
          \*******************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".change-button-block {\n    display: flex;\n    justify-content: space-evenly;\n}\n\n#change-theme-btn {\n    font: inherit;\n    border: 0;\n    outline: 0;\n    background-color: #333;\n    color: #ecf0f1;\n}\n\n#change-theme-btn:hover { \n    text-decoration: underline;\n}\n\n#change-theme-btn.dark-theme {\n    background-color: #ecf0f1;\n    color: #333;\n}\n\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGFzazUvdGFzazUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGFBQWE7SUFDYiw2QkFBNkI7QUFDakM7O0FBRUE7SUFDSSxhQUFhO0lBQ2IsU0FBUztJQUNULFVBQVU7SUFDVixzQkFBc0I7SUFDdEIsY0FBYztBQUNsQjs7QUFFQTtJQUNJLDBCQUEwQjtBQUM5Qjs7QUFFQTtJQUNJLHlCQUF5QjtJQUN6QixXQUFXO0FBQ2YiLCJmaWxlIjoic3JjL2FwcC90YXNrNS90YXNrNS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNoYW5nZS1idXR0b24tYmxvY2sge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1ldmVubHk7XG59XG5cbiNjaGFuZ2UtdGhlbWUtYnRuIHtcbiAgICBmb250OiBpbmhlcml0O1xuICAgIGJvcmRlcjogMDtcbiAgICBvdXRsaW5lOiAwO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMzMzM7XG4gICAgY29sb3I6ICNlY2YwZjE7XG59XG5cbiNjaGFuZ2UtdGhlbWUtYnRuOmhvdmVyIHsgXG4gICAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XG59XG5cbiNjaGFuZ2UtdGhlbWUtYnRuLmRhcmstdGhlbWUge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNlY2YwZjE7XG4gICAgY29sb3I6ICMzMzM7XG59XG5cbiJdfQ== */");
            /***/ 
        }),
        /***/ "./src/app/task5/task5.component.ts": 
        /*!******************************************!*\
          !*** ./src/app/task5/task5.component.ts ***!
          \******************************************/
        /*! exports provided: Task5Component */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Task5Component", function () { return Task5Component; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            var Task5Component = /** @class */ (function () {
                function Task5Component() {
                }
                Task5Component.prototype.ngOnInit = function () {
                };
                Task5Component.prototype.themeChanger = function () {
                    var hr = document.getElementsByTagName('hr');
                    var modal1 = document.getElementById('id01');
                    var modal2 = document.getElementById('id02');
                    for (var i = 0; i < hr.length; i++) {
                        hr[i].classList.toggle('dark-theme');
                    }
                    var themeBtn = document.getElementById('change-theme-btn');
                    themeBtn.classList.toggle('dark-theme');
                    modal1.classList.toggle('dark-theme');
                    modal2.classList.toggle('dark-theme');
                    document.body.classList.toggle('dark-theme');
                };
                return Task5Component;
            }());
            Task5Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-task5',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./task5.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/task5/task5.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./task5.component.css */ "./src/app/task5/task5.component.css")).default]
                })
            ], Task5Component);
            /***/ 
        }),
        /***/ "./src/app/task6/task6.component.css": 
        /*!*******************************************!*\
          !*** ./src/app/task6/task6.component.css ***!
          \*******************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".button-ctrl-block {\n    margin-top:19px;\n\n}\n\n.window{\n    margin-left: 2%;\n    margin-bottom: 2%;\n}\n\n.well {\n    width: 80%;\n    margin-bottom: .5rem;\n    padding: .5rem 1rem;\n    border: 1px dashed #ccc;\n    background-color: #f1f1f1;\n    box-sizing: border-box;\n}\n\nbutton {\n    background-color: #2199e8;\n    color: white;\n    padding: 14px 20px;\n    margin: 8px 0;\n    margin-right: 1.0625rem;\n    border: none;\n    cursor: pointer;\n    width: 100%;\n    margin-top: -3%;\n}\n\nbutton:hover {\n    opacity: 0.8;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGFzazYvdGFzazYuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGVBQWU7O0FBRW5COztBQUVBO0lBQ0ksZUFBZTtJQUNmLGlCQUFpQjtBQUNyQjs7QUFFQTtJQUNJLFVBQVU7SUFDVixvQkFBb0I7SUFDcEIsbUJBQW1CO0lBQ25CLHVCQUF1QjtJQUN2Qix5QkFBeUI7SUFDekIsc0JBQXNCO0FBQzFCOztBQUVBO0lBQ0kseUJBQXlCO0lBQ3pCLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsYUFBYTtJQUNiLHVCQUF1QjtJQUN2QixZQUFZO0lBQ1osZUFBZTtJQUNmLFdBQVc7SUFDWCxlQUFlO0FBQ25COztBQUVBO0lBQ0ksWUFBWTtBQUNoQiIsImZpbGUiOiJzcmMvYXBwL3Rhc2s2L3Rhc2s2LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYnV0dG9uLWN0cmwtYmxvY2sge1xuICAgIG1hcmdpbi10b3A6MTlweDtcblxufVxuXG4ud2luZG93e1xuICAgIG1hcmdpbi1sZWZ0OiAyJTtcbiAgICBtYXJnaW4tYm90dG9tOiAyJTtcbn1cblxuLndlbGwge1xuICAgIHdpZHRoOiA4MCU7XG4gICAgbWFyZ2luLWJvdHRvbTogLjVyZW07XG4gICAgcGFkZGluZzogLjVyZW0gMXJlbTtcbiAgICBib3JkZXI6IDFweCBkYXNoZWQgI2NjYztcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjFmMWYxO1xuICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG59XG5cbmJ1dHRvbiB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzIxOTllODtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgcGFkZGluZzogMTRweCAyMHB4O1xuICAgIG1hcmdpbjogOHB4IDA7XG4gICAgbWFyZ2luLXJpZ2h0OiAxLjA2MjVyZW07XG4gICAgYm9yZGVyOiBub25lO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBtYXJnaW4tdG9wOiAtMyU7XG59XG4gIFxuYnV0dG9uOmhvdmVyIHtcbiAgICBvcGFjaXR5OiAwLjg7XG59Il19 */");
            /***/ 
        }),
        /***/ "./src/app/task6/task6.component.ts": 
        /*!******************************************!*\
          !*** ./src/app/task6/task6.component.ts ***!
          \******************************************/
        /*! exports provided: Task6Component */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Task6Component", function () { return Task6Component; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _services_chat_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/chat.service */ "./src/app/services/chat.service.ts");
            var Task6Component = /** @class */ (function () {
                function Task6Component(chatService) {
                    var _this = this;
                    this.chatService = chatService;
                    this.user = '';
                    this.room = '';
                    this.joined = false;
                    this.messageArray = [];
                    this.chatService.newUserJoined()
                        .subscribe(function (data) { return _this.messageArray.push(data); });
                    this.chatService.userLeftRoom()
                        .subscribe(function (data) { return _this.messageArray.push(data); });
                    this.chatService.newMessageReceived()
                        .subscribe(function (data) { return _this.messageArray.push(data); });
                }
                Task6Component.prototype.ngOnInit = function () {
                };
                Task6Component.prototype.join = function () {
                    this.joined = true;
                    this.chatService.joinRoom({
                        user: this.user,
                        room: this.room
                    });
                };
                Task6Component.prototype.leave = function () {
                    this.chatService.leaveRoom({
                        user: this.user,
                        room: this.room
                    });
                    this.joined = false;
                };
                Task6Component.prototype.sendMessage = function () {
                    this.chatService.sendMessage({
                        user: this.user,
                        room: this.room,
                        message: this.messageText
                    });
                };
                Task6Component.prototype.clearInput = function () {
                    this.messageText = '';
                };
                return Task6Component;
            }());
            Task6Component.ctorParameters = function () { return [
                { type: _services_chat_service__WEBPACK_IMPORTED_MODULE_2__["ChatService"] }
            ]; };
            Task6Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-task6',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./task6.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/task6/task6.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./task6.component.css */ "./src/app/task6/task6.component.css")).default]
                })
            ], Task6Component);
            /***/ 
        }),
        /***/ "./src/app/userClass/user.ts": 
        /*!***********************************!*\
          !*** ./src/app/userClass/user.ts ***!
          \***********************************/
        /*! exports provided: User */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function () { return User; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            var User = /** @class */ (function () {
                function User() {
                }
                return User;
            }());
            /***/ 
        }),
        /***/ "./src/environments/environment.ts": 
        /*!*****************************************!*\
          !*** ./src/environments/environment.ts ***!
          \*****************************************/
        /*! exports provided: environment */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function () { return environment; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            // This file can be replaced during build by using the `fileReplacements` array.
            // `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
            // The list of file replacements can be found in `angular.json`.
            var environment = {
                production: false
            };
            /*
             * For easier debugging in development mode, you can import the following file
             * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
             *
             * This import should be commented out in production mode because it will have a negative impact
             * on performance if an error is thrown.
             */
            // import 'zone.js/dist/zone-error';  // Included with Angular CLI.
            /***/ 
        }),
        /***/ "./src/main.ts": 
        /*!*********************!*\
          !*** ./src/main.ts ***!
          \*********************/
        /*! no exports provided */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
            /* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
            /* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
            if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
            }
            Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
                .catch(function (err) { return console.error(err); });
            /***/ 
        }),
        /***/ 0: 
        /*!***************************!*\
          !*** multi ./src/main.ts ***!
          \***************************/
        /*! no static exports found */
        /***/ (function (module, exports, __webpack_require__) {
            module.exports = __webpack_require__(/*! /home/dunice/WorkSpace/angular_test/client-app/src/main.ts */ "./src/main.ts");
            /***/ 
        }),
        /***/ 1: 
        /*!********************!*\
          !*** ws (ignored) ***!
          \********************/
        /*! no static exports found */
        /***/ (function (module, exports) {
            /* (ignored) */
            /***/ 
        })
    }, [[0, "runtime", "vendor"]]]);
//# sourceMappingURL=main-es2015.js.map
//# sourceMappingURL=main-es5.js.map
//# sourceMappingURL=main-es5.js.map