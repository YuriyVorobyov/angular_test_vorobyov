#!/usr/bin/env node

/**
 * Module dependencies.
 */

const app = require('../app');

const debug = require('debug')('server-app:server');

const http = require('http');


/**
 * Create HTTP server.
 */

const server = http.createServer(app);

/**
 * Normalize a port into a number, string, or false.
 */

const normalizePort = function normalizePort(val) {
  const port = parseInt(val, 10);

  if (isNaN(port)) {

    // Named pipe
    return val;
  }

  if (port >= 0) {

    // Port number
    return port;
  }

  return false;
};

/**
 * Get port from environment and store in Express.
 */

const port = normalizePort(process.env.PORT || '3000');

/**
 * Event listener for HTTP server "error" event.
 */

const onError = function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  const bind = typeof port === 'string'
    ? `Pipe ${port}`
    : `Port ${port}`;

  // Handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(`${bind} requires elevated privileges`);
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(`${bind} is already in use`);
      process.exit(1);
      break;
    default:
      throw error;
  }
};

/**
 * Event listener for HTTP server "listening" event.
 */

const onListening = function onListening() {
  const addr = server.address();

  const bind = typeof addr === 'string'
    ? `pipe ${addr}`
    : `port ${addr.port}`;


  debug(`Listening on ${bind}`);
};

app.set('port', port);

const io = require('socket.io').listen(server);

io.on('connection', socket => {
  console.log('New connection made');

  // Joining chat
  socket.on('join', data => {
    socket.join(data.room);

    console.log(`${data.user} joined the room : ${data.room}`);

    socket.broadcast.to(data.room).emit('new user joined', { user: data.user, message: 'has joined this room' });
  });

  // Leaving chat
  socket.on('leave', data => {
    console.log(`${data.user} left the room : ${data.room}`);

    socket.broadcast.to(data.room).emit('left room', { user: data.user, message: 'has left this room' });

    socket.leave(data.room);
  });

  socket.on('message', data => {
    io.in(data.room).emit('new message', { user: data.user, message: data.message });
  });
});

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

