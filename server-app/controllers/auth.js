const jwt = require('jsonwebtoken');
const passport = require('passport');
const { User } = require('../models');

require('../config/passport')(passport);

const BAD_REQUEST = 400;
const UNAUTHORIZED = 401;

const tokenVal = 86400;
const tokenVal2 = 30;

const JWT = 'JWT ';

module.exports = {
  signUp(req, res) {
    if (!req.body.login || !req.body.password) {
      res.status(BAD_REQUEST).send({ msg: 'Please pass user information.' });
    } else {
      User
        .findOne({ where: { login: req.body.login } })
        .then(login => {
          if (login) {
            res.status(UNAUTHORIZED).send({ message: 'Registration failed. Login already in use.' });
          } else {
            User
              .create({
                login: req.body.login,
                password: req.body.password,
              })
              .then(user => {
                const token = jwt.sign(
                  JSON.parse(JSON.stringify(user)),
                  'nodeauthsecret', { expiresIn: tokenVal * tokenVal2 },
                );

                jwt.verify(token, 'nodeauthsecret', (err, data) => {
                  console.log(err, data);
                });
                res.json({
                  success: true,
                  token: JWT + token,
                  id: user.id,
                  login: user.login,
                });
              });
          }
        })
        .catch(error => {
          res.status(BAD_REQUEST).send(error);
        });
    }
  },
  signIn(req, res) {
    User
      .findOne({ where: { login: req.body.login } })
      .then(user => {
        if (!user) {
          return res.status(UNAUTHORIZED).send({ message: 'Authentication failed. User not found.' });
        }
        user.comparePassword(req.body.password, (err, isMatch) => {
          if (isMatch && !err) {
            const token = jwt.sign(
              JSON.parse(JSON.stringify(user)),
              'nodeauthsecret', { expiresIn: tokenVal * tokenVal2 },
            );

            jwt.verify(token, 'nodeauthsecret', (error, data) => {
              console.log(error, data);
            });
            res.json({
              success: true,
              token: JWT + token,
              id: user.id,
              login: user.login,
            });
          } else {
            res.status(UNAUTHORIZED).send({ success: false, msg: 'Authentication failed. Wrong password.' });
          }
        });
      })
      .catch(error => res.status(BAD_REQUEST).send(error));
  },
};
