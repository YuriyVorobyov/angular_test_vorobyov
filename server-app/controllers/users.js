const { User } = require('../models');
const OK = 200;
const BAD_REQUEST = 400;


module.exports = {
  list(req, res) {
    return User
      .findAll({ order: [['id']] })
      .then(users => res.status(OK).send(users))
      .catch(error => res.status(BAD_REQUEST).send(error));
  },
  findUser(req, res) {
    return User
      .findByPk(req.params.id)
      .then(user => res.status(OK).send(user))
      .catch(error => res.status(BAD_REQUEST).send(error));
  },
};
